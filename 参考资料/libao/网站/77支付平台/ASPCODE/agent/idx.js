function MM_reloadPage(init) {
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

var isDOM = (document.getElementById ? true : false);
var isIE4 = ((document.all && !isDOM) ? true : false);
var isNS4 = (document.layers ? true : false);
var isNS = navigator.appName == "Netscape";
function getRef(id) {
	if (isDOM) return document.getElementById(id);
	if (isIE4) return document.all[id];
	if (isNS4) return document.layers[id];
}var scrollerHeight = 88;
var puaseBetweenImages = 3000;
var imageIdx = 0;
function startVScroll() {
}
function moveRightEdge() {
	var yMenuFrom, yMenuTo, yOffset, timeoutNextCheck;

	if (isDOM) {
		yMenuFrom   = parseInt (divMenu.style.top, 10);
		yMenuTo     = (isNS ? window.pageYOffset : document.body.scrollTop)+ 30; 
	}
	timeoutNextCheck = 500;

	if (yMenuFrom != yMenuTo) {
		yOffset = Math.ceil(Math.abs(yMenuTo - yMenuFrom) / 20);
		if (yMenuTo < yMenuFrom)
			yOffset = -yOffset;
		if (isNS4)
			divMenu.top += yOffset;
		else if (isDOM)
			divMenu.style.top = parseInt (divMenu.style.top, 10) + yOffset;
			timeoutNextCheck = 10;
	}
	setTimeout ('moveRightEdge()', timeoutNextCheck);
}

function MM_swapImgRestore() { 
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { 
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { 
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { 
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-

2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a

[i+2];}
}


function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function checkReg()
{
	if (!document.reg.agree.checked)//登录代号
	{
		alert("请选择同意用户合作协议,并认真阅读该协议!");
		return false;
	}
	rememno = /\b[A-Za-z]{1}[0-9A-Za-z_]+[0-9A-Za-z]\b/;
	if (!rememno.exec(document.reg.login_name.value) || document.reg.login_name.value.length < 5  || document.reg.login_name.value.length > 20 )//登录代号
	{
		alert("登录代号必须在5-20字符之间，只能由字母，数字和下划线三种字符组成，且由字母开头，不以下划线结尾！");
		return false;
	}
pwd = /[\'\<\>\;\&\%]{1}/;
	if (pwd.exec(document.reg.passwd1.value) || document.reg.passwd1.value.length < 6  || document.reg.passwd1.value.length > 16)//密码
	{
		alert("密码必须在6-16字符之间，且不能包含一下字符：' ; % < > &");
		return false;
	}
	if (document.reg.passwd1.value != document.reg.passwd2.value)//确认密码
	{
		alert("登录密码与确认密码不符！");
		return false;
	}	
		
	if (document.reg.site_url.value == "")
	{
		alert("非法的网站地址！");
		return false;
	}
	if (document.reg.qq.value == "")
	{
		alert("请填写QQ号码，方便联系！");
		return false;
	}
if (document.reg.payee.value.length == "")//真实姓名
	{
		alert("请填写收款人姓名！");
		return false;
	}
	
	if (document.reg.bank_card.value == "")
	{
		alert("请填写银行帐号！");
		return false;
	}
	if (isNaN(reg.bank_card.value))
	{
		alert("银行帐号格式错误！");
		reg.bank_card.focus();
		return false;
	}
	if (document.reg.bank_address.value == "")
	{
		alert("请填写开户银行详细地址！");
		return false;
	}
	
	return true;
}
