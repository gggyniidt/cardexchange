﻿/** 1. 功能：公共Javascript实用脚本库
*  2. 作者：何平 
*  3. 创建日期：2008-8-10
*  4. 最后修改日期：2008-8-12
*  5. 说明：本实用脚本库从多方收集整理而成。
**/

//================================功能：将回车转成Tab======================================
//说明: 在文本框加上onkeydown事件.
//范例：<asp:TextBox ID="TextBox1" runat="server" onkeydown="EnterToTab()"></asp:TextBox>
function EnterToTab() {
    if (event.keyCode == 13)
        event.keyCode = 9;
}

//================================功能：回车后跳到指定控件======================================
//说明: 在文本框加上onkeydown事件.
//参数说明：control——控件的实例，不要传入字符串的ID.
//范例：<asp:TextBox ID="TextBox1" runat="server" onkeydown="EnterToControl(TextBox2)"></asp:TextBox>
function EnterToControl(control) {
    //如果控件为空，则返回
    if (control == null) {
        if (event.keyCode == 13)
            event.keyCode = 9;
        return;
    }
    if (event.keyCode == 13)
        control.focus();
}

//================================功能：当字符数达到指定长度，则自动跳格=============================
//说明: 在文本框加上onkeydown事件.
//参数说明：
//<1> control——控件的实例，不要传入字符串的ID.
//<2> length——自动跳格的字符长度。
//<3> reveiveFocusControl——接受焦点的控件实例,如果不传入该参数，则跳到下一个默认的控件。
//范例：<asp:TextBox ID="TextBox1" runat="server" onkeydown="AutoTab(this,7,TextBox2)"></asp:TextBox>
function AutoTab(control, length, reveiveFocusControl) {
    //如果控件为空，则返回
    if (control == null) {
        return;
    }
    //接受焦点的控件为空，则跳到下一个默认的控件
    if (reveiveFocusControl == null) {
        if (control.value.length == length && event.keyCode != 8) {
            event.keyCode = 9;
        }
    } else {
        if (control.value.length == length && event.keyCode != 8) {
            reveiveFocusControl.focus();
        }
    }
}

//================================功能：使文本框只能输入整数======================================
//说明: 在文本框加上onkeypress事件.
//范例：<asp:TextBox ID="TextBox1" runat="server" onkeypress="InputOnlyInt()"></asp:TextBox>
function InputOnlyInt() {
    //获取键盘码
    var code = event.keyCode;
    //超过整数范围则返回false
    if ((code >= 48 && code <= 57) || (code >= 33 && code <= 40) || code == 8 || code == 43 || code == 45)
        return true;
    else {
        event.keyCode = null;
        window.status = "只能输入整数！";
        return false;
    }
}

//================================功能：使文本框只能输入数字======================================
//说明: 在文本框加上onkeypress事件.
//参数说明：control——文本框的实例，不要传入字符串的ID.
//范例：
//<1> <asp:TextBox ID="TextBox1" runat="server" onkeypress="InputOnlyNumber(this)"></asp:TextBox>
function InputOnlyNumber(control) {
    //如果控件为空，则返回
    if (control == null) {
        return;
    }
    //获取键盘码
    var code = event.keyCode;
    //超过整数范围则返回false
    if ((code >= 48 && code <= 57) || (code >= 33 && code <= 40) || code == 8 || code == 43 || code == 45)
        return true;
    else if (code == 46 && control.value.indexOf('.') == -1)
        return true;
    else {
        event.keyCode = null;
        window.status = "只能输入数字！";
        return false;
    }
}

//================================功能：使文本框只能输入日期字符======================================
//说明: 在文本框加上onkeypress事件.
//范例：<asp:TextBox ID="TextBox1" runat="server" onkeypress="InputOnlyDate()"></asp:TextBox>
function InputOnlyDate() {
    //获取键盘码
    var code = event.keyCode;
    //键盘码转成字符
    var keyChar = String.fromCharCode(code);

    if ((code == null) || (code == 0) || (code == 8) || (code == 9) || (code == 13) || (code == 27)) {
        window.status = "";
        return true;
    }
    else if (("0123456789/-:").indexOf(keyChar) < 0) {
        event.keyCode = null;
        window.status = "只能输入日期！";
        return false;
    }
}

//================================功能：验证文本框的值是否为日期======================================
//说明: 在文本框加上onblur事件.
//参数说明：control——文本框的实例，不要传入字符串的ID.
//范例：<asp:TextBox ID="TextBox1" runat="server" onblur="IsDate(this)"></asp:TextBox>
function IsDate(control) {
    if (control == null) {
        return false;
    }
    var regex = /^(\d{4})(-|\/)(\d{1,2})\2(\d{1,2})$/;
    if (control.value == '')
        return true;
    if (!regex.test(control.value)) {
        alert('日期格式不正确，格式：yyyy-mm-dd或yyyy/mm/dd');
        control.focus();
        return false;
    } else {
        var r = control.value.match(regex);
        var d = new Date(r[1], r[3] - 1, r[4]);
        if (!(d.getFullYear() == r[1] && (d.getMonth() + 1) == r[3] && d.getDate() == r[4])) {
            alert('非法日期！');
            control.focus();
            return false;
        }
        else
            return true;
    }
}

//================================功能：验证文本框的值是否为日期(可包括时间)======================================
//说明: 在文本框加上onblur事件.
//参数说明：control——文本框的实例，不要传入字符串的ID.
//范例：<asp:TextBox ID="TextBox1" runat="server" onblur="IsDateTime(this)"></asp:TextBox>
function IsDateTime(control) {
    if (control == null) {
        return false;
    }
    var regex = /^([1,2]\d{3}(-|\/)\d{1,2}(-|\/)\d{1,2})? ?(\d{1,2}:\d{1,2}:\d{1,2})?$/;
    if (control.value == '')
        return true;
    if (!regex.test(control.value)) {
        alert('日期格式不正确，格式：yyyy-mm-dd hh:mm:ss 或 yyyy/mm/dd hh:mm:ss');
        control.focus();
        return false;
    } else
        return true;
}

//================================功能：验证文本框的值是否为Email======================================
//说明: 在文本框加上onblur事件.
//参数说明：control——文本框的实例，不要传入字符串的ID.
//范例：<asp:TextBox ID="TextBox1" runat="server" onblur="IsEmail(this)"></asp:TextBox>
function IsEmail(control) {
    if (control == null) {
        return false;
    }
    var regex = /^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/;
    if (control.value == '')
        return true;
    if (!regex.test(control.value)) {
        alert('电子邮件格式不正确');
        control.focus();
        return false;
    } else
        return true;
}

//================================功能：验证文本框的值是否为IP地址======================================
//说明: 在文本框加上onblur事件.
//参数说明：control——文本框的实例，不要传入字符串的ID.
//范例：<asp:TextBox ID="TextBox1" runat="server" onblur="IsIPAddress(this)"></asp:TextBox>
function IsIPAddress(control) {
    if (control == null) {
        return false;
    }
    var regex = /^((2[0-4]\d|25[0-5]|[01]?\d\d?)\.){3}(2[0-4]\d|25[0-5]|[01]?\d\d?)$/;
    if (control.value == '')
        return true;
    if (!regex.test(control.value)) {
        alert('IP地址格式不正确');
        control.focus();
        return false;
    } else
        return true;
}

//================================功能：验证文本框的值是否为身份证======================================
//说明: 在文本框加上onblur事件.
//参数说明：control——文本框的实例，不要传入字符串的ID.
//范例：<asp:TextBox ID="TextBox1" runat="server" onblur="IsIdCard(this)"></asp:TextBox>
function IsIdCard(control) {
    if (control == null) {
        return false;
    }
    var regex = /^(11|12|13|14|15|21|22|23|31|32|33|34|35|36|37|41|42|43|44|45|46|50|51|52|53|54|61|62|63|64|65|71|81|82|91)(\d{13}|\d{15}[\dx])$/;
    if (control.value == '')
        return true;
    if (!regex.test(control.value)) {
        alert('身份证格式不正确');
        control.focus();
        return false;
    } else
        return true;
}

//================================功能：显示自定义消息提示窗口======================================
//说明: 在文本框加上onfocus事件.
//<1> alt_Right是向右提示窗口DIV的ID,alt_Left是向左提示窗口DIV的ID
//<2> altMsg_Right是向右提示窗口中TD的ID,altMsg_Left是向左提示窗口中TD的ID
//参数说明：
//<1> 参数1：control——文本框的实例，不要传入字符串的ID.
//<2> 参数2：msg —— 提示消息。
//范例：<asp:TextBox ID="TextBox1" runat="server" onfocus="DisplayMsg(this,"嘿嘿")"></asp:TextBox>
function DisplayMsg(control, msg) {
    var t = control.offsetTop;
    var l = control.offsetLeft;
    while (control = control.offsetParent) {
        l += control.offsetLeft;
        t += control.offsetTop;
    }

    if (l > 800) {
        //显示箭头向右的DIV
        alt_Right.style.left = l - 300;
        alt_Right.style.position = "absolute";
        alt_Right.style.top = t;
        altMsg_Right.innerHTML = msg;
        alt_Right.style.display = "";
    } else {
        //显示箭头向左的DIV
        alt_Left.style.left = l + 165;
        alt_Left.style.position = "absolute";
        alt_Left.style.top = t;
        altMsg_Left.innerHTML = msg;
        alt_Left.style.display = "";
    }
}

//================================功能：隐藏自定义消息提示窗口======================================
function HideMsg() {
    alt_Left.style.display = "none";
    alt_Right.style.display = "none";
}

//==================================================== 文件上传  =========================================
//上传控件初始ID索引
var initID = 1;

//文件上传主函数
function UploadFile() {
    //当前文件控件
    var inputFile = document.getElementById("file" + initID);

    //创建下方的div
    CreateDiv();

    //将文件信息显示到下方的表格中
    ShowFileInfo(inputFile);

    //检测添加的文件是否重复,如果重复则删除前面那个节点
    CheckExists(inputFile);

    //动态创建文件上传控件
    CreateFileControl();
}

//创建显示文件信息的容器DIV
function CreateDiv() {
    var divTag = document.getElementById("div_Info");
    if ((divTag == null) || (divTag == undefined)) {
        divTag = "<div id='div_Info' class='Div_Style1'></div>";
        document.getElementById("table_UploadFile").insertAdjacentHTML("afterEnd", divTag);
    }
}

//显示文件信息
function ShowFileInfo(control) {
    //创建一个层,表示一行
    var div_row = document.createElement("div");
    //显示文件信息和删除链接
    var tag = "<span class='Attach'>&nbsp;&nbsp;</span>&nbsp;";
    tag += "<span >" + GetFileName(control.value) + "</span>&nbsp;";
    tag += "<a class='Alink' id='A_File_" + initID + "' onclick='DeleteContainer(this)'>删除</a>";
    //将信息插入上面的层中
    div_row.innerHTML = tag;
    //将文件控件也插入该层中
    div_row.appendChild(control);
    //将该层插入div_Info中
    document.getElementById("div_Info").appendChild(div_row);
}

//获取文件路径的文件名( 包含扩展名 )
function GetFileName(filePath) {
    fileName = filePath.split("\\");
    return fileName[fileName.length - 1];
}

//删除文件控件所在的div
function DeleteContainer(control) {
    document.getElementById("div_Info").removeChild(control.parentElement);
    //如果div_Info所有的子div都被删除了,则删除div_Info
    if (document.getElementById("div_Info").childNodes.length == 0) {
        document.getElementById("div_Info").parentNode.removeChild(document.getElementById("div_Info"));
    }
}

//检测添加的文件是否重复,如果重复则删除前面那个节点
function CheckExists(control) {
    //获取div_Info节点
    var div_Info = document.getElementById("div_Info");
    //将重复的节点删除
    for (var i = 0; i < div_Info.childNodes.length; i++) {
        for (var j = 0; j < div_Info.childNodes[i].childNodes.length; j++) {
            //获取div中的控件
            var fileControl = div_Info.childNodes[i].childNodes[j];
            if (fileControl.type != "file") {
                continue;
            }
            //比对
            if (fileControl != control) {
                if (fileControl.value == control.value) {
                    DeleteContainer(fileControl);
                }
            }
        }
    }
}

//创建文件上传控件
function CreateFileControl() {
    //上传控件初始ID索引加1
    initID++;
    //创建文件上传控件的标记
    var input_File = "<input type='file' name='file" + initID + "' id='file" + initID + "' size='0' class='Input_File' onchange='UploadFile()' />";
    document.getElementById("file_Container").insertAdjacentHTML("beforeEnd", input_File);
}

//================================== 按钮提交后立即失效 ===============================================
function ButtonPostingStop(btn) {
    //保存按钮的原值
    var btnText = btn.value;
    //禁用按钮
    btn.disabled = true;
    //设置失效时按钮的文本
    btn.value = "请稍候";

    try {
        WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(btn.id, '', true, '', '', false, false));
        //提交完成，恢复按钮的原始状态
        if (!WebForm_OnSubmit()) {
            btn.disabled = false;
            btn.value = btnText;
        }
    }
    catch (e) {
    }
}
