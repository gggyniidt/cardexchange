function checkLogin(form)
{
	if(InValidValue(form.loginName,"登录用户名不能为空")||
		InValidValue(form.loginPass,"登录密码不能为空")||
		InValidValue(form.checkCode,"请输入验证码") )
		return false;
	else
		return true;
}


function checkUserInfoModifyPass(form)
{
	if( InValidValue(form.newPass,"请输入新密码")||
		InValidValue(form.rePass,"请再输入一次新密码") )
		return false;
	
	if(form.newPass.value!=form.rePass.value)
	{
		alert("两次输入的密码不唯一！");
		form.rePass.focus();
		form.rePass.select();
		return false;
	}
	
	return true;
}


//*********************
function checkModifyPass(form)
{
	if( InValidValue(form.oldPass,"请输入原始密码")||
		InValidValue(form.newPass,"请输入新密码")||
		InValidValue(form.rePass,"请再输入一次新密码") )
		return false;
	
	if(form.newPass.value!=form.rePass.value)
	{
		alert("两次输入的密码不唯一！");
		form.rePass.focus();
		form.rePass.select();
		return false;
	}
	
	return true;
}

//yes
function checkBankForUser(form)
{
	var p=/^\d+(\.\d+)?$/;
	if(!p.test(form.payMoney.value))
	{
		alert("结算金额必须是数字");	
		form.payMoney.focus();
		form.payMoney.select();
		return false;
	}
	var balance=parseFloat(form.balance.value);
	if(balance<=0)
	{
		alert("余额不足,无法结算");return false;
	}
	var payMoney=parseFloat(form.payMoney.value);
	if(payMoney==0)
	{
		alert("结算金额必须大于零");	return false;
	}
	if(payMoney>balance)
	{
		alert("输入的结算金额大于其余额！");form.payMoney.focus();return false;
	}
	return checkPay();
}

//yes
function BizBankForComAskPay(form)
{
	if(!isNumeric(form.payMoney,"提现金额必须是数字"))return false;
	
	var balance=parseFloat(form.balance.value);
	var tx0=parseFloat(form.tx0.value);
	var tx1=parseFloat(form.tx1.value);
	var payMoney=parseFloat(form.payMoney.value);
	
	if(payMoney<=0)
	{
		alert("申请支付金额必须大于0");return false;
	}

	if( payMoney > balance)
	{
		alert("您输入的提现申请金额超过您账户余额，操作被取消！");
		form.payMoney.focus();
		return false;
	}
	
	var txType=parseInt(form.txType.value);
	
	try
	{
		var pTx=0;
		if(txType==0)//立即提现
		{
			if(payMoney<tx0){alert("您的余额不够支付手续费，操作被取消");return false;}
		}
		else
		{
			if(payMoney<tx1){alert("您的余额不够支付手续费，操作被取消");return false;}
		}	
	}
	catch(e){}
	return confirm("是否确定申请提现？？");	
}

//yes
function checkChannel(form)
{
	if(InValidValue(form.channelName,"通道名称不能为空")||
		!isNumeric(form.commission,"提成比例必须是小数"))
		return false;
	else
	{
		if(!isCommissionForUsers(form.commission))
		{
			return confirm("提成比例超过95%，是否要继续？");
		}
		return true;		
	}
}

function checkDoMethod(form)
{
	if(InValidValue(form.methodName,"名称不能为空"))
		return false;
	else
		return true;
}

function checkChannelForUser(form)
{
	if(!isNumeric(form.commission,"提成比例必须是小数"))
		return false;
	else
	{
		return confirm("请检查‘销售分成比例’，是否大于‘代理分成比例’，是否要继续？");
	}
}

//yes
function checkAdminModifyPass(form)
{
	if(InValidValue(form.newPass,"新密码不能为空")||
		InValidValue(form.rePass,"请重新输入一次新密码"))
		return false;

	if(form.newPass.value!=form.rePass.value)
	{
		alert("您两次输入的密码不唯一");return false;
	}
	return true;
}


//yes
function checkAdminAdd(form)
{
	if(InValidValue(form.adminName,"登录用户名不能为空")||
		InValidValue(form.adminPass,"登录密码不能为空")||
		InValidValue(form.trueName,"管理者真实姓名不能为空"))
		return false;
	else
	{
		return true;
	}	
}

function isNumeric(obj,msg)
{
	if(InValidValue(obj,msg))return false;
	var p=/^\d+(\.\d+)?$/;
	if(!p.test(obj.value))
	{
		alert(msg);
		obj.focus();
		obj.select();
		return false;
	}
	return true;
}

function isCommission(oa,ob)
{
	var a=parseFloat(oa.value);
	var b=parseFloat(ob.value);
	if(a>b)return false;
	return true;
}

function isCommissionForSales(obj)
{
	//alert(val);
	var a=parseFloat(obj.value);	
	if(a>=0.1)return false;
	return true;
}

function isCommissionForUsers(obj)
{
	//alert(val);
	var a=parseFloat(obj.value);	
	if(a>0.95)return false;
	return true;
}

function checkPay()
{
	return confirm("确定支付嘛？");	
}

function InValidValue(obj,msg)
{
	if(obj.value=="")
	{
		obj.focus();
		alert(msg);
		return true;
	}
	return false;
}

function askDel()
{
	return confirm("此操作不可恢复,确定要删除嘛?");	
}

function showHidden(id)
{
	var obj=document.getElementById("msg_"+id);	
	if(obj.style.display=="none")
		obj.style.display="block";
	else
		obj.style.display="none"
}

