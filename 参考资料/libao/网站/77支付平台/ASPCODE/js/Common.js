﻿//对页面事件做简单的处理,上传的时候请注意去掉下面三个 return false;前的注释
document.onselectstart = function(){
	var obj = window.event.srcElement;
	if (obj.type != "text" && obj.type != "textarea" && obj.type != "password" && obj.type != "file"){
		//return false;
	}
}
document.ondragstart = function(){
		//return false;
}
document.oncontextmenu = function(){
	var obj = window.event.srcElement;
	if (obj.type != "text" && obj.type != "textarea" && obj.type != "password" && obj.type != "file"){
		//return false;
	}
}
window.onerror = function(){
	//return true;
}

function getobjAbsolutePos (obj) {
	var r = { x: obj.offsetLeft, y: obj.offsetTop };
	if (obj.offsetParent) {
		var tmp = Calendar.getAbsolutePos(obj.offsetParent);
		r.x += tmp.x;
		r.y += tmp.y;
	}
	return r;
}

function setClassName(obj, css) {
    var os = document.getElementById(obj);
    if (os == null) return;
    os.className = css;
}

function JHshStrLen(sString){				//汉字作为2个字节的字符串长度检测
	var sStr,iCount,i,strTemp ;
	iCount = 0 ;
	sStr = sString.split("");
	for (i = 0 ; i < sStr.length ; i ++){
		strTemp = escape(sStr[i]);
		if (strTemp.indexOf("%u",0) == -1){ 	 // 表示是汉字
			iCount = iCount + 1 ;
		}else{
			iCount = iCount + 2 ;
		}
	}
	return iCount ;
}
function isDate(sDate) {
	var iYear, iMonth, iDay, iIndex

	var	reg
	reg = new RegExp('[^0-9-]','')
	if (sDate.search(reg) >= 0)
		return false;
	
	iIndex = sDate.indexOf('-');
	if ( iIndex == -1 )
		return false;
	else {
		iYear = parseFloat(sDate.substr(0, iIndex));
		if ( isNaN(iYear) || iYear < 1900 || iYear > 2099 )
			return false;
		else
			sDate = sDate.substring(iIndex + 1, sDate.length);
	}
	
	iIndex = sDate.indexOf('-');
	if ( iIndex == -1 )
		return false;
	else {
		iMonth = parseFloat(sDate.substr(0, iIndex));
		if ( isNaN(iMonth) || iMonth < 1 || iMonth > 12 )
			return false;
		else
			sDate = sDate.substring(iIndex + 1, sDate.length);
	}
	
	iIndex = sDate.indexOf('-');
	if ( iIndex >= 0 )
		return false;
	else {
		iDay = parseFloat(sDate);
		if ( isNaN(iDay) || iDay < 1 || iDay > 31 )
			return false;
	}
	
	
	switch(iMonth) {
		case 4:
		case 6:
		case 9:
		case 11:
			if ( iDay > 30 )
				return false;
			else
				break;
		case 2:
			if ( ( ( iYear % 4 == 0 && iYear % 100 != 0 ) || iYear % 400 == 0 ) && iDay > 29 )
				return false;
			else if ( (iYear % 4 != 0 || (iYear % 100 == 0 && iYear % 400 != 0)) && iDay > 28 )
				return false;
			else
				break;
		default:
	}
	return true;
}

function isDateString(sDate){
	var iaMonthDays = [31,28,31,30,31,30,31,31,30,31,30,31]
	var iaDate = new Array(3)
	var year, month, day

	if (arguments.length != 1) return false
	iaDate = sDate.toString().split("-")
	if (iaDate.length != 3) return false
	if (iaDate[1].length > 2 || iaDate[2].length > 2) return false

	year = parseFloat(iaDate[0])
	month = parseFloat(iaDate[1])
	day=parseFloat(iaDate[2])

	if (year < 1900 || year > 2100) return false
	if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) iaMonthDays[1]=29;
	if (month < 1 || month > 12) return false
	if (day < 1 || day > iaMonthDays[month - 1]) return false
	return true
}

function stringToDate(sDate, bIgnore){
	var bValidDate, year, month, day
	var iaDate = new Array(3)
	
	if (bIgnore) bValidDate = true
	else bValidDate = isDateString(sDate)
	
	if (bValidDate)
	{  iaDate = sDate.toString().split("-")
		year = parseFloat(iaDate[0])
		month = parseFloat(iaDate[1]) - 1
		day=parseFloat(iaDate[2])
		return (new Date(year,month,day))
	}
	else return (new Date(1900,1,1))
}

function trim(str) {
	var i=0;
	var j=str.length;
	for (i=0; i<str.length; i++)
	if (str.charAt(i)!=' ')  break;
	for (j=str.length-1; j>=0; j--)
	if (str.charAt(j)!=' ')  break;
	if (i>j)  return "";
	return str.substring(i, j+1);
}

function isEmpty(obj) {
	obj.value = trim(obj.value);    
	if (obj.value.length <= 0)  return(true);
	return(false);
}


function checkBrowser(){ 
	this.ver=navigator.appVersion 
	this.dom=document.getElementById?1:0 
	this.ie6=(this.ver.indexOf("MSIE 6")>-1 && this.dom)?1:0; 
	this.ie5=(this.ver.indexOf("MSIE 5")>-1 && this.dom)?1:0; 
	this.ie4=(document.all && !this.dom)?1:0; 
	this.ns5=(this.dom && parseInt(this.ver) >= 5) ?1:0; 
	this.ns4=(document.layers && !this.dom)?1:0; 
	this.mac=(this.ver.indexOf('Mac') > -1) ?1:0; 
	this.ope=(navigator.userAgent.indexOf('Opera')>-1); 
	this.ie=(this.ie6 || this.ie5 || this.ie4) 
	this.ns=(this.ns4 || this.ns5) 
	this.bw=(this.ie6 || this.ie5 || this.ie4 || this.ns5 || this.ns4 || this.mac || this.ope) 
	this.nbw=(!this.bw) 
	return this;
}


//对String类型的一些扩充，方便在脚本中使用 部分正则表达式来自网络

String.prototype.LTrim	= function(){
	return this.replace(/(^\s*)/g, "");
}

String.prototype.rTrim	= function(){
	return this.replace(/(\s*$)/g, "");
}

String.prototype.Trim	= function(){
	return this.replace(/(^\s*)|(\s*$)/g, "");
}

//写入Flash
function WriteFlash(Path,Width,Height,Transparent){
	var Temp,T=""
	Temp='<object classid="clsid:D27CDB6E-AE6D-11CF-96B8-444553540000" id="FlashH" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" border="0" width="'+Width+'" height="'+Height+'">'
	Temp+='<param name="movie" value="'+Path+'"/>'
	Temp+='<param name="quality" value="High"/>'
	Temp+='<param name="scale" value="ExactFit"/>'
	if (Transparent) {Temp+=' <param name="wmode" value="transparent"/>';T='wmode="transparent"'}
	Temp+='<embed src="'+Path+'" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" name="FlashH" width="'+Width+'" height="'+Height+'" quality="High"'+T+' scale="ExactFit"/>'
	Temp+='</object>'
	document.write(Temp);
	//document.getElementById("pic_box").innerHTML=Temp
}
//创建xmlhtp对象
var isIE = false;
function xmlHTTP(){
	if (window.XMLHttpRequest){ //非IE
		return new XMLHttpRequest();
	} else if (window.ActiveXObject){ // IE
		isIE = true;
		return new ActiveXObject("Microsoft.XMLHttp");
	}
}

//设置文章字体大小
function SetFont(size){
	document.getElementById("artBox").style.fontSize=size
}


function ReImgSize(){
  for (i=0;i<document.images.length;i++)
   {
   if (document.all){
	if (document.images[i].width>150)
	 {
       document.images[i].width="150"
       document.images[i].outerHTML='<a href="'+document.images[i].src+'" target="_blank" title="在新窗口打开图片">'+document.images[i].outerHTML+'</a>'
  	 }
   }
  else{
	if (document.images[i].width>150) {
	  document.images[i].title="在新窗口打开图片"
	  document.images[i].style.cursor="pointer"
	  document.images[i].onclick=function(e){window.open(this.src)}
	}
  }
  }
 }

//在页面中插入Flash
function insertFlash(Path,Width,Height,Transparent,Scale){
	 var Temp,T=""
	 Temp='<object classid="clsid:D27CDB6E-AE6D-11CF-96B8-444553540000" id="FlashH" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" border="0" width="'+Width+'" height="'+Height+'">'
	 Temp+='<param name="movie" value="'+Path+'"/>'
	 Temp+='<param name="quality" value="High"/>'
	 if (Transparent) {Temp+=' <param name="wmode" value="transparent"/>';T='wmode="transparent"'}
	 if (!Scale) {
	 //scale参数设置为ExactFit时可能会引起舞台显示不完整
		Temp+='<param name="scale" value="ExactFit"/>'
		Temp+='<embed src="'+Path+'" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" name="FlashH" width="'+Width+'" height="'+Height+'" quality="High" '+T+' scale="ExactFit"/>'
	 }else{
		//使用默认的scale参数showAll时舞台不能被缩放
		Temp+='<embed src="'+Path+'" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" name="FlashH" width="'+Width+'" height="'+Height+'" quality="High" '+T+' />'
	 }
	 Temp+='</object>'
	 document.write(Temp);
}

  /***
  * jquery实现的弹出消息框
  * 需要引用：jquery-1.3.2.min.js 和   jquery-ui-1.7.2.custom.min.js 
  **/
function DialogMessage(title, content) {
    var dialogId = "Mydialog";
    var dialogTitle = title;
    var dialogContent = content;
    var dialogJqueryId = "#" + dialogId;

    var divDialog = $("<div id=" + dialogId + " title='" + dialogTitle + "'><p>" + dialogContent + "</p></div>");

    if ($(dialogJqueryId).length == 0) {
        $(document.body).append(divDialog);
    }
    else {
        $(dialogJqueryId).attr("title", dialogTitle);
        $(dialogJqueryId).text(dialogContent);
    }
   
    $(dialogJqueryId).dialog({
        autoOpen: false,
        width: 400,
        bgiframe: false,
        modal: true,
        buttons: {
            "确定": function() {
                $(this).dialog("close");
            }
        }
    });
    $(dialogJqueryId).dialog('open');
    return false;
}

/***
** 需要引用：jquery-1.3.2.min.js 和   jquery-ui-1.7.2.custom.min.js 
********************************************************************
* jQuery使用IFrame
* ControlName:产生Dialog弹出框的控件名称
* DialogTitme:Diglog标题
* IFrameSrc ：IFrame连接的URL地址
* IFrameHeight: IFrame高度
* IFrameWidth ：IFrame宽度
********************************************************************
**/
function DialogIFrame(ControlName, DialogCssClass, DialogTitle, IFrameSrc, DialogWidth, DialogHeight) {
    $(ControlName).click(function(e) {
        e.preventDefault();
        var $this = $(this);
        var horizontalPadding = 30;
        var verticalPadding = 30;
        var IFrameWidth = DialogWidth - horizontalPadding;
        var IFrameHeight = DialogHeight - verticalPadding;
        var iframe = $('<iframe id="externalSite" border="0"  class="' + DialogCssClass + '" src="' + IFrameSrc + '" />');

        $(iframe).dialog({
            title: ($this.attr('title')) ? $this.attr('title') : DialogTitle,
            autoOpen: true,
            width: DialogWidth,
            height: DialogHeight,
            modal: true,
            resizable: true,
            autoResize: true
        }).width(IFrameWidth).height(IFrameHeight);
    });
};

function gid(id){return document.getElementById?document.getElementById(id):null;}
function mcl(show, div, btn, over, padd){var objdiv = gid(div);var objbtn = gid(btn);if (objdiv && objbtn){var browser = new Browser();if (show){objdiv.style.display = "block";if (browser.isIE && over){var allselect = gname("select");for (var i=0; i<allselect.length; i++){allselect[i].style.visibility = "hidden";}}objdiv.style.top = (objbtn.offsetTop + objbtn.offsetHeight - 2) + "px";objdiv.style.left = (objbtn.offsetLeft - (padd?0:1)) + "px";}else{objdiv.style.display = "none";if (browser.isIE && over){var allselect = gname("select");for (var i=0; i<allselect.length; i++){allselect[i].style.visibility = "visible";}}}}}

function Browser(){var ua, s, i;this.isIE = false;this.isNS = false;this.isOP = false;this.isSF = false;ua = navigator.userAgent.toLowerCase();s = "opera";if ((i = ua.indexOf(s)) >= 0){this.isOP = true;return;}s = "msie";if ((i = ua.indexOf(s)) >= 0) {this.isIE = true;return;}s = "netscape6/";if ((i = ua.indexOf(s)) >= 0) {this.isNS = true;return;}s = "gecko";if ((i = ua.indexOf(s)) >= 0) {this.isNS = true;return;}s = "safari";if ((i = ua.indexOf(s)) >= 0) {this.isSF = true;return;}}
function gname(name){return document.getElementsByTagName?document.getElementsByTagName(name):new Array()}
