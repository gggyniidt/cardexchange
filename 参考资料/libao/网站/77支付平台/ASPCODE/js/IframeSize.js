﻿var getFFVersion = navigator.userAgent.substring(navigator.userAgent.indexOf("Firefox")).split("/")[1]
//extra height in px to add to iframe in FireFox 1.0+ browsers   
var FFextraHeight = getFFVersion >= 0.1 ? 16 : 0

function dynIframeSize(iframename) {
    var pTar = null;
    var h = 500;
    if (document.getElementById) {
        pTar = document.getElementById(iframename);
    }
    else {
        eval('pTar = ' + iframename + ';');
    }
    if (pTar == null) return;

    //begin resizing iframe   
    pTar.style.display = "block"
    if (pTar.contentDocument && pTar.contentDocument.body.offsetHeight) {
        //ns6 syntax   
        h = pTar.contentDocument.body.offsetHeight + FFextraHeight;
    }
    else if (pTar.Document && pTar.Document.body.scrollHeight) {
        //ie5+ syntax  
        h = pTar.Document.body.scrollHeight;
    }
	/*
    if (h > 500) {
        pTar.height = h;
    }
    else {
        pTar.height = 500;
    }
	*/
	pTar.height = h;
	
    pTar.width = pTar.Document.body.scrollWidth;

}