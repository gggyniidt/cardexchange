unit DBScanner;

interface

uses winsvc, Windows, SysUtils, ShellAPI, IniFiles, Forms, Dialogs, classes,
  sockets, generics.collections, SyncObjs, ActiveX, DB, adodb, UniProvider,
  ODBCUniProvider, Utils, AccessUniProvider, DBAccess, Uni, MemDS, UniDacVcl,
  SQLServerUniProvider, MySQLUniProvider, Loger;

type

  TOnDBChangeEvent = function(var rec: TDBRecord): Boolean of object;

  TDBScanner = class(TThread)
  private
    FUQ: TUniQuery;
    FTimeSpace: Integer;
    FSQL: string;
    FUC: TUniConnection;
    FUQCheck: TUniQuery;
    FOnDBChange: TOnDBChangeEvent;
    procedure ProcessData();
  public
    constructor Create(ConnStr: string);
    destructor Destroy; override;
    procedure Execute; override;
    property OnDBChange: TOnDBChangeEvent read FOnDBChange write FOnDBChange;
    property TimeSpace: Integer read FTimeSpace write FTimeSpace;
    function RealBill(Num: string): Boolean;
  end;

implementation

{ TDBScanner }

constructor TDBScanner.Create(ConnStr: string);
begin
  inherited Create(True);
  FUC := TUniConnection.Create(nil);
  ConnectDB(FUC, ConnStr);

  FUQCheck := TUniQuery.Create(nil);
  FUQCheck.Connection := FUC;

  FUQ := TUniQuery.Create(nil);
  FUQ.Connection := FUC;
  FSQL := 'select top 10 * from MirMoUrl2 where flag=0  ORDER BY id ASC';
end;

destructor TDBScanner.Destroy;
begin

  FUQCheck.Close;
  FUQCheck.Free;
  FUC.Close;
  FUC.Free;
end;

procedure TDBScanner.Execute;
begin
  LogToFile('Execute 001');
  inherited;
  while not Terminated do
  begin
    LogToFile('Execute 002');
    try
      FUQ.Close;
      FUQ.SQL.Text := FSQL;
      FUQ.Open;
      if (FUQ.RecordCount > 0) then
      begin
        ProcessData;
        FUQ.Close;
      end
      else
        Sleep(TimeSpace);

    except
      LogToFile('Execute exception');
    end;
    LogToFile('Execute 003');
  end;
end;

procedure TDBScanner.ProcessData;
var
  rec: TDBRecord;
  res: Boolean;
begin

  while not FUQ.Eof do
  begin
    with rec, FUQ do
    begin
      try
        LogToFile('ProcessData 001');
        id := FieldByName('id').AsInteger;
        codeNum := FieldByName('codeNum').AsInteger;
        UserName := FieldByName('UserName').AsString;
        payMoney := FieldByName('payMoney').AsCurrency;
        postKey := FieldByName('postKey').AsString;
        svrIp := FieldByName('svrIp').AsString;
        svrPort := FieldByName('svrPort').AsInteger;
        flag := FieldByName('flag').AsInteger;
        sendDate := FieldByName('sendDate').AsDateTime;
        give := FieldByName('give').AsInteger;
        Num := FieldByName('num').AsString;
        orderId := FieldByName('orderId').AsString;
        regName := FieldByName('regName').AsString;
        msg := FieldByName('message').AsString;
        sendFlag := FieldByName('sendFlag').AsInteger;
        classid := FieldByName('classid').AsInteger;
        userid := FieldByName('userid').AsInteger;
        serverKey := FieldByName('serverKey').AsString;

        FUQ.Edit;
        res := False;
        if Assigned(FOnDBChange) then
          res := FOnDBChange(rec);

        FieldByName('Flag').AsInteger := flag;
        FieldByName('message').AsString := msg;


        // if res then 
        // begin 
        // FieldByName('Flag').AsInteger := 1; 
        // FieldByName('message').AsString := '�ɹ�'; 
        // end 
        // else 
        // begin 
        // FieldByName('Flag').AsInteger := 2; 
        // FieldByName('message').AsString := rec.msg; 
        // end; 

        FUQ.Post;
        LogToFile('ProcessData 002');
      except
        on E: Exception do
        begin
          // ShowMessage(orderId); 
          LogToFile('ProcessData Exception' + E.Message);
        end;
      end;

      FUQ.Next;

    end;
    // ShowMessage('SelectCount:' + IntToStr(FUQ.RecordCount) 
    // + #13 + 'InQueue:' + IntToStr(FDBScanner.FDBRecQueue.Count)); 

  end;
end;

function TDBScanner.RealBill(Num: string): Boolean;
begin
  FUQCheck.Close;
  FUQCheck.SQL.Text := 'select * from mobile where num=' + QuotedStr(Num);
  FUQCheck.Open;
  if FUQCheck.RecordCount > 0 then
    Result := True
  else
    Result := False;
  FUQCheck.Close;
end;

end.
