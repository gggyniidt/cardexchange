program CardExchangeServer;

uses
  SvcMgr,
  Service in 'Service.pas' { Svc_CardExchangeServer: TService } ,
  Utils in '..\..\common\Utils.pas',
  global in '..\global.pas',
  DBScanner in 'DBScanner.pas',
  Loger in '..\..\Common\Loger.pas',
  Crypt in '..\..\Common\Crypt.pas',
  DataDef in '..\..\Common\DataDef.pas';
{$R *.RES}


begin
  // Windows 2003 Server requires StartServiceCtrlDispatcher to be
  // called before CoRegisterClassObject, which can be called indirectly
  // by Application.Initialize. TServiceApplication.DelayInitialize allows
  // Application.Initialize to be called from TService.Main (after
  // StartServiceCtrlDispatcher has been called).
  //
  // Delayed initialization of the Application object may affect
  // events which then occur prior to initialization, such as
  // TService.OnCreate. It is only recommended if the ServiceApplication
  // registers a class object with OLE and is intended for use with
  // Windows 2003 Server.
  //
  // Application.DelayInitialize := True;
  //
  if not Application.DelayInitialize or Application.Installing then
    Application.Initialize;
  Application.CreateForm(TSvc_CardExchangeServer, Svc_CardExchangeServer);
  //Svc_CardExchangeServer.ServiceStart(Svc_CardExchangeServer, b);
  Application.Run;

end.
