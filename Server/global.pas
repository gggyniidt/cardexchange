unit global;

interface

uses SysUtils, Classes, DB, ADODB, UniProvider, ODBCUniProvider,
  AccessUniProvider, DBAccess, Uni, MemDS, SQLServerUniProvider,
  MySQLUniProvider, Utils;

const
  ServiceName = 'Svc_CardExchangeServer';
  ServiceAppName = 'CardExchangeServer.exe';

type
  TConfig = record
    ConnStr: string;
    LogPort: string;
    ConfigConnStr: string;
    ScanTime: Integer;
    ResendTimes: Integer;
  end;

  // GUID={B08ACAED-5152-453D-8184-627F7649B102}
  // seed1=177134672
  // seed2=  166535701
  // seeed3=   90331436
var
  config: TConfig;

function LoadConfig: Boolean;
function SaveConfig: Boolean;

implementation

function SaveConfig: Boolean;
var
  UC: TUniConnection;
  UT: TUniTable;
begin
  UC := TUniConnection.Create(nil);
  UT := TUniTable.Create(nil);
  try
    try
      ConnectDB(UC, config.ConfigConnStr);
      UT.Connection := UC;
      UT.TableName := 'ServiceConfig';
      UT.Open;
      UT.Edit;
      UT.FieldByName('ConnStr').AsString := config.ConnStr;
      UT.FieldByName('LogPort').AsString := config.LogPort;
      UT.FieldByName('ScanTime').AsInteger := config.ScanTime;
      UT.FieldByName('ResendTimes').AsInteger := config.ResendTimes;
      UT.Post;
      UT.Close;
      UC.Close;
      Result := True;
    except
      Result := False;
    end;
  finally
    UT.Free;
    UC.Free
  end;

end;

function LoadConfig: Boolean;
var
  UC: TUniConnection;
  UT: TUniTable;
begin

  UC := TUniConnection.Create(nil);
  UT := TUniTable.Create(nil);
  try
    try
      ConnectDB(UC, config.ConfigConnStr);
      UT.Connection := UC;
      UT.TableName := 'ServiceConfig';
      UT.Open;
      config.ConnStr := UT.FieldByName('ConnStr').AsString;
      config.LogPort := UT.FieldByName('LogPort').AsString;
      config.ScanTime := UT.FieldByName('ScanTime').AsInteger;
      config.ResendTimes := UT.FieldByName('ResendTimes').AsInteger;
      UT.Close;
      UC.Close;
      Result := True;
    except
      Result := False;
    end;
  finally
    UT.Free;
    UC.Free;
  end;
end;

initialization

config.ConfigConnStr := 'ProviderName=Access;Username=;Password=;Server=;Port=0;Database=' + AppPath + 'config.mdb';

end.
