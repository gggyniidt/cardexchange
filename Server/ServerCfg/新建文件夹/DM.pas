unit DM;

interface

uses
  SysUtils, Classes, DB, ADODB, utils, UniProvider, ODBCUniProvider,
  AccessUniProvider, DBAccess, Uni, MemDS, UniDacVcl, SQLServerUniProvider,
  MySQLUniProvider;

type
  TFrm_DM = class(TDataModule)
    UT_ServiceConfig: TUniTable;
    UC_Config: TUniConnection;
    AccessUniProvider1: TAccessUniProvider;
    ODBCUniProvider1: TODBCUniProvider;
    UT_Partition: TUniTable;
    UC_1: TUniConnection;
    MySQLUniProvider1: TMySQLUniProvider;
    SQLServerUniProvider1: TSQLServerUniProvider;
    UCD_1: TUniConnectDialog;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Frm_DM: TFrm_DM;

implementation

{$R *.dfm}

uses global;

end.
