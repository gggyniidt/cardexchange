object Frm_DM: TFrm_DM
  OldCreateOrder = False
  Height = 423
  Width = 460
  object UT_ServiceConfig: TUniTable
    TableName = 'ServiceConfig'
    Connection = UC_Config
    Active = True
    Left = 48
    Top = 80
  end
  object UC_Config: TUniConnection
    ProviderName = 'Access'
    Database = 'd:\CardExchange\bin\server\config.mdb'
    Connected = True
    LoginPrompt = False
    Left = 144
    Top = 80
  end
  object AccessUniProvider1: TAccessUniProvider
    Left = 152
    Top = 8
  end
  object ODBCUniProvider1: TODBCUniProvider
    Left = 224
    Top = 8
  end
  object UT_Partition: TUniTable
    TableName = 'Partition'
    Connection = UC_Config
    Left = 48
    Top = 136
  end
  object UC_1: TUniConnection
    ConnectDialog = UCD_1
    Left = 152
    Top = 272
  end
  object MySQLUniProvider1: TMySQLUniProvider
    Left = 368
    Top = 88
  end
  object SQLServerUniProvider1: TSQLServerUniProvider
    Left = 368
    Top = 16
  end
  object UCD_1: TUniConnectDialog
    DatabaseLabel = #25968#25454#24211
    PortLabel = #31471#21475
    ProviderLabel = #25968#25454#24211#31867#22411
    Caption = #36830#25509#25968#25454#24211
    UsernameLabel = #29992#25143#21517
    PasswordLabel = #23494#30721
    ServerLabel = #26381#21153#22120'IP'
    ConnectButton = #36830#25509
    CancelButton = #21462#28040
    LabelSet = lsCustom
    Left = 152
    Top = 336
  end
end
