unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, Mask, Math, ExtCtrls, Menus, ADODB, Sockets, DB,
  Utils, WinSock, Loger, RzEdit, RzLabel, RzStatus, RzPanel, RzButton, DBClient,
  Grids, DBGrids, RzDBGrid, RzGrids, regware4, MemDS, DBAccess, Uni, DateUtils;

type
  TFrm_Main = class(TForm)
    Button4: TButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GroupBox2: TGroupBox;
    TrayIcon: TTrayIcon;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    Label4: TLabel;
    Label5: TLabel;
    RzNumericEdit3: TRzNumericEdit;
    RzNumericEdit4: TRzNumericEdit;
    RzStatusBar1: TRzStatusBar;
    RzToolbar1: TRzToolbar;
    Btn_Install: TRzToolButton;
    Btn_Start: TRzToolButton;
    Label9: TLabel;
    Edit1: TEdit;
    Button6: TButton;
    GroupBox3: TGroupBox;
    Timer1: TTimer;
    Label1: TLabel;
    Label13: TLabel;
    Label12: TLabel;
    Label2: TLabel;
    RegCodeBox: TEdit;
    EmailBox: TEdit;
    OrganizationBox: TEdit;
    LicenseIDBox: TEdit;
    DoRegistrationBtn: TButton;
    RegIV: TRegware4;
    DBGrid1: TDBGrid;
    UniConnection1: TUniConnection;
    UniDataSource1: TUniDataSource;
    UniQuery1: TUniQuery;
    DateTimePicker1: TDateTimePicker;
    Label7: TLabel;
    DateTimePicker2: TDateTimePicker;
    Edit2: TEdit;
    RzToolButton2: TRzToolButton;
    RzToolButton1: TRzToolButton;
    RzSpacer1: TRzSpacer;
    Label8: TLabel;
    Label10: TLabel;
    CheckBox1: TCheckBox;
    UniQuery1ID: TIntegerField;
    UniQuery1CodeNum: TIntegerField;
    UniQuery1UserName: TWideStringField;
    UniQuery1payMoney: TFloatField;
    UniQuery1Flag: TIntegerField;
    UniQuery1SendDate: TDateTimeField;
    UniQuery1OrderID: TWideStringField;
    UniQuery1Message: TWideStringField;
    UniQuery1EventName: TWideStringField;
    UniQuery1LogTime: TDateTimeField;
    Label11: TLabel;
    RzSpacer2: TRzSpacer;
    RzSpacer3: TRzSpacer;
    RzSpacer4: TRzSpacer;
    Label6: TLabel;
    UniQuery1ClientIP: TWideStringField;
    procedure Button4Click(Sender: TObject);
    procedure TrayIconDblClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure N2Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Btn_InstallClick(Sender: TObject);
    procedure Btn_StartClick(Sender: TObject);
    procedure RzToolButton2Click(Sender: TObject);
    procedure RzToolButton1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure DoRegistrationBtnClick(Sender: TObject);
    procedure SetUnregisteredBtnClick(Sender: TObject);
    procedure Edit2Change(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure DateTimePicker1Change(Sender: TObject);
    procedure DateTimePicker2Change(Sender: TObject);
    procedure UniQuery1FlagGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);

  private
    s: TSocket;
    acThreadID: DWORD;
    procedure ButtonUpdate();
    procedure GetData;
    procedure SetData;
    procedure InitLog();
    procedure GetRegInfo;
    procedure query();
  public

  end;

var
  Frm_Main: TFrm_Main;

implementation

{$R *.dfm}

uses global;

procedure TFrm_Main.Btn_InstallClick(Sender: TObject);
begin
  if ServiceUninstalled(ServiceName) then // 未安装
    ExecnWait(ServiceAppName, ' /install')
  else
    ExecnWait(ServiceAppName, ' /uninstall'); // 已安装

  ButtonUpdate;
end;

procedure TFrm_Main.Btn_StartClick(Sender: TObject);
var
  I: Integer;
begin
  if ServiceRunning(ServiceName) then
    ExecnWait('net', 'stop ' + ServiceName)
  else
    ExecnWait('net', 'start ' + ServiceName);

  ButtonUpdate;
end;

procedure TFrm_Main.Button4Click(Sender: TObject);
begin
  SetData;
  SaveConfig;

  ShowMessage('请注意，本次修改需在重启服务后生效，只需点击网关上的“停止/启动”按钮即可');
end;

procedure TFrm_Main.Button6Click(Sender: TObject);
begin
  Edit1.Text := DBConnDlg;
end;

procedure TFrm_Main.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caNone;
  Hide;
end;

procedure TFrm_Main.FormCreate(Sender: TObject);
begin
  LoadConfig();
  InitLog;
  GetRegInfo;
end;

procedure TFrm_Main.FormShow(Sender: TObject);
begin
  GetData;
  ButtonUpdate;
  DateTimePicker1.Date := StrToDate(DateToStr(Now));
  DateTimePicker1.Time := StrToTime('0:00:00');
  DateTimePicker2.Date := DateTimePicker1.Date;
  DateTimePicker2.Time := StrToTime('0:00:00');
  query;
end;

procedure TFrm_Main.GetData;
begin
  Edit1.Text := config.ConnStr;
  // RzNumericEdit2.Text := config.LogPort;
  RzNumericEdit3.Value := config.ScanTime;
  RzNumericEdit4.Value := config.ResendTimes;
end;

procedure TFrm_Main.GetRegInfo;
begin
  if RegIV.Registered then
  begin
    Label6.Caption := '已注册';
    LicenseIDBox.Text := RegIV.LicenseID;
    OrganizationBox.Text := RegIV.Organization;
    EmailBox.Text := RegIV.Email;
    RegCodeBox.Text := RegIV.RegCode;
    LicenseIDBox.Enabled := False;
    OrganizationBox.Enabled := False;
    EmailBox.Enabled := False;
    RegCodeBox.Enabled := False;
  end
  else
  begin
    LicenseIDBox.Enabled := True;
    OrganizationBox.Enabled := True;
    EmailBox.Enabled := True;
    RegCodeBox.Enabled := True;
    if RegIV.Expired then
      Label6.Caption := '未注册'
    else
      Label6.Caption := IntToStr(RegIV.DaysLeft);
  end;
end;

procedure TFrm_Main.InitLog;
begin
  ConnectDB(UniConnection1, config.ConfigConnStr);
  query;
end;

procedure TFrm_Main.N1Click(Sender: TObject);
begin
  Show;
end;

procedure TFrm_Main.N2Click(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TFrm_Main.query;
begin
  UniQuery1.Close;
  if CheckBox1.Checked then
    UniQuery1.SQL.Text := 'select * from log where logtime>=#' + DateToStr
      (DateTimePicker1.Date) + '# and logtime<=#' + DateToStr
      (IncDay(DateTimePicker2.Date)) + '# order by logtime desc'
  else
    UniQuery1.SQL.Text := 'select * from log order by logtime desc';
  UniQuery1.Open;
end;

procedure TFrm_Main.CheckBox1Click(Sender: TObject);
begin
  if CheckBox1.Checked then
  begin
    DateTimePicker1.Enabled := True;
    DateTimePicker2.Enabled := True;
  end
  else
  begin
    DateTimePicker1.Enabled := False;
    DateTimePicker2.Enabled := False;
  end;
  query;

end;

procedure TFrm_Main.DateTimePicker1Change(Sender: TObject);
begin
  DateTimePicker2.DateTime := IncDay(DateTimePicker1.DateTime);
  query;
end;

procedure TFrm_Main.DateTimePicker2Change(Sender: TObject);
begin
  query;
end;

procedure TFrm_Main.DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if DataCol <> 4 then
    exit;
  if DBGrid1.DataSource.DataSet.RecordCount <= 0 then
    exit;

  if DBGrid1.DataSource.DataSet.FieldByName('Flag').AsInteger = 1 then
    DBGrid1.Canvas.TextOut(Rect.Left, Rect.Top, '成功')
  else
    DBGrid1.Canvas.TextOut(Rect.Left, Rect.Top, '失败');
end;

procedure TFrm_Main.DoRegistrationBtnClick(Sender: TObject);
begin
  RegIV.DoRegistration(LicenseIDBox.Text, OrganizationBox.Text,
    RegCodeBox.Text, EmailBox.Text);

  GetRegInfo;
end;

procedure TFrm_Main.Edit2Change(Sender: TObject);
begin
  if Edit2.Text = '' then
  begin
    UniQuery1.Filter := '';
    UniQuery1.Filtered := False;
  end
  else
  begin
    UniQuery1.Filter := 'OrderID Like ' + QuotedStr('%' + Edit2.Text + '%')
      + ' or UserName Like ' + QuotedStr('%' + Edit2.Text + '%')
      + ' or EventName Like ' + QuotedStr('%' + Edit2.Text + '%')
      + ' or ClientIP Like ' + QuotedStr('%' + Edit2.Text + '%');
    UniQuery1.Filtered := True;
  end;
end;

procedure TFrm_Main.RzToolButton1Click(Sender: TObject);
var
  row: Integer;
begin
  for row := 0 to UniQuery1.RecordCount - 1 do
    UniQuery1.Delete;
end;

procedure TFrm_Main.RzToolButton2Click(Sender: TObject);
begin
  // UniTable1.Close;
  // UniTable1.Open;
  // ShowMessage(IntToStr(UniTable1.RecordCount));
  query;
end;

procedure TFrm_Main.SetData;
begin
  config.ConnStr := Edit1.Text;
  // config.LogPort := RzNumericEdit2.Text;
  config.ScanTime := StrToInt(RzNumericEdit3.Text);
  config.ResendTimes := StrToInt(RzNumericEdit4.Text);
end;

procedure TFrm_Main.SetUnregisteredBtnClick(Sender: TObject);
begin
  // RegIV.SetUnregistered;
  // GetRegInfo;
end;

procedure TFrm_Main.Timer1Timer(Sender: TObject);
begin
  if RegIV.Registered then
  begin
    Label6.Font.Color := clBlack;
    Label6.Caption := '已经注册！';
  end
  else
  begin
    if RegIV.Expired then
      Label6.Caption := '已经超期，请注册。'
    else
    begin
      Label6.Font.Color := clBlue;
      Label6.Caption := '未注册，请注册！';
      Caption := '服务管理器' + '  ' + IntToStr(RegIV.DaysLeft);
    end;
  end;
  // RzURLLabel1.Caption := IntToStr(UniQuery1.RecordCount);
end;

procedure TFrm_Main.TrayIconDblClick(Sender: TObject);
begin
  Show;
end;

procedure TFrm_Main.UniQuery1FlagGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  if Sender.FieldName = 'Flag' then
    DisplayText := False;
end;

procedure TFrm_Main.ButtonUpdate;
begin
  if ServiceUninstalled(ServiceName) then
  // 未安装
  begin
    Btn_Install.Caption := '安装服务';
    Btn_Start.Enabled := False;
  end
  else
  begin
    Btn_Install.Caption := '卸载服务'; // 已安装
    Btn_Start.Enabled := True;
  end;

  if ServiceRunning(ServiceName) then // 运行中
    Btn_Start.Caption := '停止'
  else
    Btn_Start.Caption := '启动'; // 停止
end;

end.
