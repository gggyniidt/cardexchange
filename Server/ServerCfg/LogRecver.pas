unit LogRecver;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, generics.collections, StdCtrls, Sockets, utils, DB, ADODB, WinSock, Loger;

type
  TLogRecver = class(TObject)
  private
    FListBox: TListBox;
    FSocket: TSocket;
    FClientsList: TList;
    function init(LogPort: Integer): Boolean;
  public
    constructor Create(ListBox: TListBox; LogPort: string);
    destructor Destroy(); override;

  end;

procedure ServerAccept(s: TSocket); stdcall;
procedure SocketWorkThread(ns: TSocket); stdcall;

implementation

uses global;

{ LogRecver }

constructor TLogRecver.Create(ListBox: TListBox; LogPort: string);
begin
  inherited Create();
  FListBox := ListBox;
  FClientsList := TList.Create;
end;

destructor TLogRecver.Destroy;
begin

  inherited;
end;

function TLogRecver.init(LogPort: Integer): Boolean;
var
  wsa: TWSAData;
  wsstatus: Integer;
  sa: sockaddr_in;
  acThreadID: DWORD;
begin
  wsstatus := WSAStartup($0202, wsa);
  if wsstatus <> 0 then
  begin
    ShowMessage('初始化socket出错！');
    Exit;
  end;

  FSocket := Socket(AF_INET, SOCK_STREAM, 0);
  if FSocket < 0 then
  begin
    ShowMessage('创建socket出错！');
    WSACleanup;
    Exit;
  end;

  sa.sin_port := htons(1389);
  sa.sin_family := AF_INET;
  sa.sin_addr.S_addr := INADDR_ANY;
  wsstatus := bind(FSocket, sa, SizeOf(sa));
  if wsstatus <> 0 then
  begin
    ShowMessage('绑定socket出错');
    WSACleanup;
    Exit;
  end;

  wsstatus := listen(FSocket, 5);
  if wsstatus <> 0 then
  begin
    ShowMessage('监听出错！');
    WSACleanup;
    Exit;
  end;

  CreateThread(nil, 0, @ServerAccept, Pointer(FSocket), 0, acThreadID);
end;

procedure ServerAccept(s: TSocket); stdcall;
begin

end;

procedure SocketWorkThread(ns: TSocket); stdcall;
begin

end;

end.
