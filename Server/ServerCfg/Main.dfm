object Frm_Main: TFrm_Main
  Left = 0
  Top = 0
  Caption = #26381#21153#31649#29702#22120
  ClientHeight = 462
  ClientWidth = 484
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 484
    Height = 443
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = #26381#21153#29366#24577
      object RzToolbar1: TRzToolbar
        Left = 0
        Top = 0
        Width = 476
        Height = 54
        BorderInner = fsNone
        BorderOuter = fsGroove
        BorderSides = [sdTop]
        BorderWidth = 0
        TabOrder = 0
        ToolbarControls = (
          Label8
          Btn_Install
          Btn_Start
          RzSpacer1
          Label10
          CheckBox1
          DateTimePicker1
          Label7
          DateTimePicker2
          Label11
          RzSpacer4
          Edit2
          RzSpacer2
          RzToolButton2
          RzSpacer3
          RzToolButton1)
        object Btn_Install: TRzToolButton
          Left = 40
          Top = 2
          Width = 60
          Hint = 'Play'
          DisabledIndex = 1
          GradientColorStyle = gcsSystem
          ImageIndex = 0
          ShowCaption = True
          UseToolbarShowCaption = False
          UseToolbarVisualStyle = False
          VisualStyle = vsWinXP
          Caption = #23433#35013#26381#21153
          OnClick = Btn_InstallClick
        end
        object Btn_Start: TRzToolButton
          Left = 100
          Top = 2
          Width = 65
          Hint = 'Stop'
          DisabledIndex = 5
          ImageIndex = 4
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          Caption = #21551#21160'/'#20572#27490
          OnClick = Btn_StartClick
        end
        object Label7: TLabel
          Left = 345
          Top = 8
          Width = 24
          Height = 13
          Caption = '  '#33267'  '
        end
        object RzToolButton2: TRzToolButton
          Left = 365
          Top = 27
          Width = 36
          ShowCaption = True
          UseToolbarShowCaption = False
          Caption = #21047#26032
          OnClick = RzToolButton2Click
        end
        object RzToolButton1: TRzToolButton
          Left = 409
          Top = 27
          Width = 60
          ShowCaption = True
          UseToolbarShowCaption = False
          Caption = #28165#31354#26085#24535
          OnClick = RzToolButton1Click
        end
        object RzSpacer1: TRzSpacer
          Left = 165
          Top = 2
          Grooved = True
        end
        object Label8: TLabel
          Left = 4
          Top = 8
          Width = 36
          Height = 13
          Caption = #26381#21153#65306
        end
        object Label10: TLabel
          Left = 173
          Top = 8
          Width = 30
          Height = 13
          Caption = #26085#24535'  '
        end
        object Label11: TLabel
          Left = 4
          Top = 33
          Width = 132
          Height = 13
          Caption = #36755#20837#35746#21333#21495#25110#29992#25143#21517#26597#25214
        end
        object RzSpacer2: TRzSpacer
          Left = 357
          Top = 27
        end
        object RzSpacer3: TRzSpacer
          Left = 401
          Top = 27
        end
        object RzSpacer4: TRzSpacer
          Left = 136
          Top = 27
        end
        object DateTimePicker1: TDateTimePicker
          Left = 251
          Top = 4
          Width = 94
          Height = 21
          Date = 41175.158545601850000000
          Time = 41175.158545601850000000
          TabOrder = 0
          OnChange = DateTimePicker1Change
        end
        object DateTimePicker2: TDateTimePicker
          Left = 369
          Top = 4
          Width = 100
          Height = 21
          Date = 41175.158545601850000000
          Time = 41175.158545601850000000
          TabOrder = 1
          OnChange = DateTimePicker2Change
        end
        object Edit2: TEdit
          Left = 144
          Top = 29
          Width = 213
          Height = 21
          DoubleBuffered = True
          ParentDoubleBuffered = False
          TabOrder = 2
          OnChange = Edit2Change
        end
        object CheckBox1: TCheckBox
          Left = 203
          Top = 6
          Width = 48
          Height = 17
          Caption = #26102#38388
          Checked = True
          State = cbChecked
          TabOrder = 3
          OnClick = CheckBox1Click
        end
      end
      object DBGrid1: TDBGrid
        Left = 0
        Top = 54
        Width = 476
        Height = 361
        Align = alClient
        DataSource = UniDataSource1
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        ReadOnly = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnDrawColumnCell = DBGrid1DrawColumnCell
        Columns = <
          item
            Expanded = False
            FieldName = 'EventName'
            Title.Caption = #20107#20214
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Message'
            Title.Caption = #28040#24687
            Width = 86
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ClientIP'
            Title.Caption = #23458#25143#31471'IP'
            Width = 82
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OrderID'
            Title.Caption = #35746#21333#21495
            Width = 125
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Flag'
            Title.Caption = #32467#26524
            Width = 27
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'payMoney'
            Title.Caption = #37329#39069
            Width = 33
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'UserName'
            Title.Caption = #29609#23478
            Width = 81
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CodeNum'
            Title.Caption = #20998#21306
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SendDate'
            Title.Caption = #21457#36865#26102#38388
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LogTime'
            Title.Caption = #35760#24405#26102#38388
            Visible = True
          end>
      end
    end
    object TabSheet2: TTabSheet
      Caption = #26381#21153#35774#32622
      ImageIndex = 1
      object GroupBox2: TGroupBox
        Left = 0
        Top = 3
        Width = 483
        Height = 78
        Caption = #32593#32476#35774#32622
        TabOrder = 0
        object Label4: TLabel
          Left = 15
          Top = 50
          Width = 171
          Height = 13
          Caption = #25968#25454#25195#25551#38388#38548'                         '#27627#31186
        end
        object Label5: TLabel
          Left = 212
          Top = 50
          Width = 162
          Height = 13
          Caption = #25968#25454#37325#21457#27425#25968'                          '#27425
        end
        object Label9: TLabel
          Left = 16
          Top = 16
          Width = 72
          Height = 13
          Caption = #25968#25454#24211#36830#25509#20018
        end
        object RzNumericEdit3: TRzNumericEdit
          Left = 93
          Top = 46
          Width = 65
          Height = 21
          MaxLength = 5
          TabOrder = 2
          Max = 60000.000000000000000000
          Min = 60.000000000000000000
          DisplayFormat = '0;(0)'
        end
        object RzNumericEdit4: TRzNumericEdit
          Left = 290
          Top = 46
          Width = 65
          Height = 21
          MaxLength = 1
          TabOrder = 3
          Max = 5.000000000000000000
          Min = 1.000000000000000000
          DisplayFormat = '0;(0)'
        end
        object Edit1: TEdit
          Left = 94
          Top = 12
          Width = 345
          Height = 21
          ReadOnly = True
          TabOrder = 1
        end
        object Button6: TButton
          Left = 445
          Top = 10
          Width = 25
          Height = 25
          Caption = '...'
          TabOrder = 0
          OnClick = Button6Click
        end
        object Button4: TButton
          Left = 395
          Top = 44
          Width = 75
          Height = 25
          Caption = #20445#23384#35774#32622
          TabOrder = 4
          OnClick = Button4Click
        end
      end
      object GroupBox3: TGroupBox
        Left = 3
        Top = 87
        Width = 480
        Height = 130
        Caption = #27880#20876#20449#24687
        TabOrder = 1
        object Label1: TLabel
          Left = 48
          Top = 73
          Width = 35
          Height = 13
          Caption = #27880#20876'ID'
        end
        object Label13: TLabel
          Left = 7
          Top = 24
          Width = 76
          Height = 13
          Caption = #32452#32455#21517'/'#29992#25143#21517
        end
        object Label12: TLabel
          Left = 35
          Top = 49
          Width = 48
          Height = 13
          Caption = #30005#23376#37038#20214
        end
        object Label2: TLabel
          Left = 47
          Top = 98
          Width = 36
          Height = 13
          Caption = #27880#20876#30721
        end
        object Label6: TLabel
          Left = 370
          Top = 29
          Width = 97
          Height = 21
          AutoSize = False
          Caption = 'Label6'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object RegCodeBox: TEdit
          Left = 91
          Top = 96
          Width = 271
          Height = 21
          TabOrder = 3
        end
        object EmailBox: TEdit
          Left = 91
          Top = 48
          Width = 271
          Height = 21
          TabOrder = 1
        end
        object OrganizationBox: TEdit
          Left = 91
          Top = 24
          Width = 271
          Height = 21
          TabOrder = 0
        end
        object LicenseIDBox: TEdit
          Left = 91
          Top = 72
          Width = 271
          Height = 21
          TabOrder = 2
        end
        object DoRegistrationBtn: TButton
          Left = 370
          Top = 88
          Width = 97
          Height = 25
          Caption = #27880#20876
          Default = True
          TabOrder = 4
          OnClick = DoRegistrationBtnClick
        end
      end
    end
  end
  object RzStatusBar1: TRzStatusBar
    Left = 0
    Top = 443
    Width = 484
    Height = 19
    BorderInner = fsNone
    BorderOuter = fsNone
    BorderSides = [sdLeft, sdTop, sdRight, sdBottom]
    BorderWidth = 0
    TabOrder = 1
  end
  object TrayIcon: TTrayIcon
    Hint = #26381#21153#31649#29702#22120
    PopupMenu = PopupMenu1
    Visible = True
    OnDblClick = TrayIconDblClick
    Left = 32
    Top = 376
  end
  object PopupMenu1: TPopupMenu
    Left = 88
    Top = 376
    object N1: TMenuItem
      Caption = #26174#31034#26381#21153#31649#29702#22120
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = #36864#20986
      OnClick = N2Click
    end
  end
  object Timer1: TTimer
    OnTimer = Timer1Timer
    Left = 144
    Top = 376
  end
  object RegIV: TRegware4
    ProgGUIDEx = '{B08ACAED-5152-453D-8184-627F7649B102}'
    Seed1 = 177134672
    Seed2 = 166535701
    Seed3 = 90331436
    LicenseOptions.LicenseSource = lsUserDefined
    LicenseOptions.LicenseType = ltSingleUserLicense
    Left = 216
    Top = 96
  end
  object UniConnection1: TUniConnection
    ProviderName = 'Access'
    Database = 'F:\CardExchange_Home\bin\Server\config.mdb'
    LoginPrompt = False
    Left = 120
    Top = 304
  end
  object UniDataSource1: TUniDataSource
    DataSet = UniQuery1
    Left = 256
    Top = 304
  end
  object UniQuery1: TUniQuery
    Connection = UniConnection1
    SQL.Strings = (
      'select * from log')
    Left = 192
    Top = 304
    object UniQuery1ID: TIntegerField
      AutoGenerateValue = arAutoInc
      FieldName = 'ID'
      ReadOnly = True
    end
    object UniQuery1CodeNum: TIntegerField
      FieldName = 'CodeNum'
    end
    object UniQuery1UserName: TWideStringField
      FieldName = 'UserName'
      Size = 100
    end
    object UniQuery1payMoney: TFloatField
      FieldName = 'payMoney'
    end
    object UniQuery1ClientIP: TWideStringField
      FieldName = 'ClientIP'
      Size = 15
    end
    object UniQuery1Flag: TIntegerField
      FieldName = 'Flag'
      OnGetText = UniQuery1FlagGetText
    end
    object UniQuery1SendDate: TDateTimeField
      FieldName = 'SendDate'
    end
    object UniQuery1OrderID: TWideStringField
      FieldName = 'OrderID'
      Size = 50
    end
    object UniQuery1Message: TWideStringField
      FieldName = 'Message'
      Size = 255
    end
    object UniQuery1EventName: TWideStringField
      FieldName = 'EventName'
      Size = 50
    end
    object UniQuery1LogTime: TDateTimeField
      FieldName = 'LogTime'
    end
  end
end
