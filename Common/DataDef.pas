unit DataDef;

interface

uses Utils;

const
  CryptKey = 'Powerd by �ඡ  QQ:2385261255';
  // ��������ע����� ����ɾ��
  // AuthCode = 'GYIWYTCXJ';
  // Days = 10;
  // ProgGUID = '{F02E4374-382B-4165-A91E-9C89EE4AF69F}';
  // Seed = 13483426905;

type
  TCEWhich = (cewData, cewError, cewSucc);

  TCEPtl_U = record
    case Integer of
      0:
        (Rec: TDBRecord; );
      1:
        (errcode: Integer;
          ErrMsg: string[50]);
      2:
        (flag: Integer;
          Succmsg: string[50]; );

  end;

  TCEPtl = packed record
    which: TCEWhich;
    u: TCEPtl_U;
  end;

  PCEPtl = ^TCEPtl;

  TCRYPTTYPE = (ctNone, ctAES128, ctAES192, ctAES256, ctDES, ctXOR);

  TNetPck = record
    PACKSIZE: Integer;
    CEPTL: TCEPtl;
    CRYPTTYPE: TCRYPTTYPE;
    CHECKSUM: array [0 .. 15] of Byte;
  end;

  PNetPck = ^TNetPck;

implementation

end.
