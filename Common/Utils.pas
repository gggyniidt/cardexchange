unit Utils;

interface

uses winsvc, Windows, SysUtils, ShellAPI, Forms, Dialogs, classes,
  sockets, SyncObjs, adodb, Uni, Winsock, StrUtils,
  IdHashMessageDigest, IdGlobal, IdHash, UniDacVcl;

var
  AppName: string;
  AppPath: string;

  { 判断某服务是否安装，未安装返回true，已安装返回false }
function ServiceUninstalled(sService: string): boolean;
{ 判断某服务是否启动，启动返回true，未启动返回false }
function ServiceRunning(sService: string): boolean;
{ 判断某服务是否停止，停止返回true，未停止返回false }
function ServiceStopped(sService: string): boolean;

// 判断端口(Port)是否被占用 在使用Socks编程时 ， 有时需要判断某个端口是否被占用 ， 也就是是否被其它程序使用 。

function IsPortUsed(aPort: Integer): boolean;
function GetHostName: String;
procedure ExecnWait(AppName, Para: string);
procedure Log(s: string);

type
  TDBRecord = record
    id: Integer;
    codeNum: Integer;
    UserName: string[100];
    payMoney: Currency;
    postKey: string[100];
    svrIp: string[20];
    svrPort: Integer;
    flag: Integer;
    sendDate: TDateTime;
    give: Integer;
    num: string[50];
    orderId: string[50];
    regName: string[50];
    msg: string[50];
    sendFlag: Integer;
    classid: Integer;
    userid: Integer;
    serverKey: string[50];
  end;

  TTCPClientWatcher = class(TThread)
  protected
    procedure Execute; override;
  public
    TCPClient: TTcpClient;
    RestartFlag: boolean;
  end;

function DBConnDlg(): string;
procedure ConnectDB(UC: TUniConnection; ConnStr: string); overload;
procedure ConnectDB(UC: TUniConnection; ConnStr: string; UseUnicode: boolean);
  overload;

implementation

procedure ConnectDB(UC: TUniConnection; ConnStr: string; UseUnicode: boolean);
  overload;
  procedure SetUnicode(UC: TUniConnection; UseUnicode: boolean);
  begin
    if UseUnicode then
    begin
      if ContainsText('Access,MySQL,ODBC', UC.ProviderName) then
        UC.SpecificOptions.Values[UC.ProviderName + '.UseUnicode'] := 'True';
      if UC.ProviderName = 'MySQL' then
        UC.SpecificOptions.Values[UC.ProviderName + '.Charset'] := 'utf8';
    end
    else
    begin
      if ContainsText('Access,MySQL,ODBC', UC.ProviderName) then
        UC.SpecificOptions.Values[UC.ProviderName + '.UseUnicode'] := 'False';
      if UC.ProviderName = 'MySQL' then
        UC.SpecificOptions.Values[UC.ProviderName + '.Charset'] := '';
    end;
  end;

var
  sl: TStringList;
  temp: string;
begin
  sl := TStringList.Create;

  while Pos(';', ConnStr) > 0 do
  begin
    temp := Copy(ConnStr, 1, Pos(';', ConnStr) - 1);
    sl.Add(temp);
    Delete(ConnStr, 1, Pos(';', ConnStr));
  end;
  sl.Add(ConnStr);

  UC.LoginPrompt := False;
  UC.ProviderName := sl.Values['ProviderName'];
  UC.UserName := sl.Values['Username'];
  UC.Password := sl.Values['Password'];
  UC.Server := sl.Values['Server'];
  UC.Port := StrToInt(sl.Values['Port']);
  UC.Database := sl.Values['Database'];
  SetUnicode(UC, UseUnicode);
  UC.Connect;
  sl.Free;
end;

procedure ConnectDB(UC: TUniConnection; ConnStr: string);
var
  sl: TStringList;
  temp: string;
begin
  sl := TStringList.Create;

  while Pos(';', ConnStr) > 0 do
  begin
    temp := Copy(ConnStr, 1, Pos(';', ConnStr) - 1);
    sl.Add(temp);
    Delete(ConnStr, 1, Pos(';', ConnStr));
  end;
  sl.Add(ConnStr);

  UC.LoginPrompt := False;
  UC.ProviderName := sl.Values['ProviderName'];
  UC.UserName := sl.Values['Username'];
  UC.Password := sl.Values['Password'];
  UC.Server := sl.Values['Server'];
  UC.Port := StrToInt(sl.Values['Port']);
  UC.Database := sl.Values['Database'];
  UC.Connect;
  sl.Free;
end;

function DBConnDlg(): string;
var
  UC: TUniConnection;
  UCD: TUniConnectDialog;
  ConnStr: string;
begin
  UC := TUniConnection.Create(nil);
  UCD := TUniConnectDialog.Create(nil);

  with UCD do
  begin
    CancelButton := '取消';
    Caption := '连接数据库';
    ConnectButton := '连接';
    DatabaseLabel := '数据库';
    PasswordLabel := '密码';
    PortLabel := '端口';
    ProviderLabel := '数据库类型';
    ServerLabel := '数据库IP';
    UsernameLabel := '用户名';
  end;

  UC.ConnectDialog := UCD;

  UC.Disconnect;
  if UC.ConnectDialog.Execute then
  begin
    ConnStr := 'ProviderName=' + UC.ProviderName;
    ConnStr := ConnStr + #29 + 'Username=' + UC.UserName;
    ConnStr := ConnStr + #29 + 'Password=' + UC.Password;
    ConnStr := ConnStr + #29 + 'Server=' + UC.Server;
    ConnStr := ConnStr + #29 + 'Port=' + inttostr(UC.Port);
    ConnStr := ConnStr + #29 + 'Database=' + UC.Database;
  end;
  UC.Disconnect;
  ConnStr := StringReplace(ConnStr, #29, ';', [rfReplaceAll]);
  Result := ConnStr;
end;

function IsPortUsed(aPort: Integer): boolean;
var
  _vSock: TSocket;
  _vWSAData: TWSAData;
  _vAddrIn: TSockAddrIn;
begin
  Result := False;
  if WSAStartup(MAKEWORD(2, 2), _vWSAData) = 0 then
  begin
    _vSock := Socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    try
      if _vSock <> SOCKET_ERROR then
      begin
        _vAddrIn.sin_family := AF_INET;
        _vAddrIn.sin_addr.S_addr := htonl(INADDR_ANY);
        _vAddrIn.sin_port := htons(aPort);
        if Bind(_vSock, _vAddrIn, SizeOf(_vAddrIn)) <> 0 then
          if WSAGetLastError = WSAEADDRINUSE then
            Result := True;
      end;
    finally
      CloseSocket(_vSock);
      WSACleanup();
    end;
  end;
end;

procedure Log(s: string);
var
  sl: TStringList;
  LogFileName: string;
begin
  LogFileName := 'c:\tt.log';
  // LogFileName := ChangeFileExt(Application.ExeName, '.log');
  sl := TStringList.Create;
  if not FileExists(LogFileName) then
    sl.SaveToFile(LogFileName);

  sl.LoadFromFile(LogFileName);
  sl.Add(s);
  sl.SaveToFile(LogFileName);
  sl.Free;
end;

procedure ExecnWait(AppName, Para: string);
var
  sei: SHELLEXECUTEINFO;
begin
  // 启动进程
  ZeroMemory(@sei, SizeOf(SHELLEXECUTEINFO));
  sei.cbSize := SizeOf(SHELLEXECUTEINFO);
  sei.fMask := SEE_MASK_NOCLOSEPROCESS;
  sei.Wnd := 0;
  sei.lpVerb := nil;
  sei.lpFile := pwchar(AppName);
  sei.lpParameters := pwchar(Para);
  sei.lpDirectory := nil;
  sei.nShow := SW_HIDE;
  sei.hInstApp := 0;
  ShellExecuteEx(@sei);
  // 加入下面这句就是等待该进程结束
  WaitForSingleObject(sei.hProcess, INFINITE);
  // 也可以直接关闭这个进程，只要保留sei.hProcess就行
  TerminateProcess(sei.hProcess, 0);

end;

function GetHostName: String;
var
  ComputerName: array [0 .. MAX_COMPUTERNAME_LENGTH + 1] of char;
  Size: Cardinal;

begin
  Result := '';
  Size := MAX_COMPUTERNAME_LENGTH + 1;
  GetComputerName(ComputerName, Size);
  Result := StrPas(ComputerName);
end;

// 取系统服务的状态
function ServiceGetStatus(sService: string): DWord;
var
  schm, schs: SC_Handle;
  ss: TServiceStatus;
  dwStat: DWord;
  sMachine: string;
begin
  sMachine := GetHostName;
  dwStat := 0;
  schm := OpenSCManager(PChar(sMachine), Nil, SC_MANAGER_CONNECT);
  if (schm > 0) then
  begin
    schs := OpenService(schm, PChar(sService), SERVICE_QUERY_STATUS);
    if (schs > 0) then
    begin
      if (QueryServiceStatus(schs, ss)) then
        dwStat := ss.dwCurrentState;
      CloseServiceHandle(schs);
    end;
    CloseServiceHandle(schm);
  end;
  Result := dwStat;
end;

{ 判断某服务是否安装，未安装返回true，已安装返回false }
function ServiceUninstalled(sService: string): boolean;
begin
  Result := 0 = ServiceGetStatus(sService);
end;

{ 判断某服务是否启动，启动返回true，未启动返回false }
function ServiceRunning(sService: string): boolean;
begin
  Result := SERVICE_RUNNING = ServiceGetStatus(sService);
end;

{ 判断某服务是否停止，停止返回true，未停止返回false }
function ServiceStopped(sService: string): boolean;
begin
  Result := SERVICE_STOPPED = ServiceGetStatus(sService);
end;

{ TTCPClientWatcher }

procedure TTCPClientWatcher.Execute;
begin
  inherited;

  while (True) do
  begin
    Sleep(100);
    if RestartFlag then
    begin
      TCPClient.Active := False;
      TCPClient.Active := True;
      RestartFlag := False;
    end;
  end;

end;

initialization

// CoInitialize(nil);
AppName := Application.ExeName;
AppPath := ExtractFilePath(AppName);

finalization

// CoUninitialize;

end.
