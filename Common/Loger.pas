unit Loger;

interface

uses winsvc, Windows, SysUtils, ShellAPI, IniFiles, Forms, Dialogs, classes,
  sockets, generics.collections, SyncObjs, adodb, ActiveX, Uni, Winsock,
  IdHashMessageDigest, IdGlobal, IdHash, UniDacVcl, global, Utils;

type
  TLogRecord = record
    CodeNum: Integer;
    UserName: string[100];
    PayMoney: Currency;
    YuanBao: Currency;
    Flag: Integer;
    SendDate: TDateTime;
    OrderID: string[50];
    MSG: string[255];
    EventName: string[50];
    LogTime: TDateTime;
    ServerIP: string[15];
    ClientIP: string[15];
  end;

  TLogWriteDB = class(TThread)
  private
    FUC: TUniConnection;
    FUQ: TUniQuery;
    FUQISSended: TUniQuery;
    FConnStr: string;
    FCSLogDB: TCriticalSection;
    FDBQueue: TQueue<TLogRecord>;
    procedure WriteLog(Rec: TLogRecord);
    procedure SetConnStr(const Value: string);
  public
    property ConnStr: string read FConnStr write SetConnStr;
    property DBQueue: TQueue<TLogRecord>read FDBQueue write FDBQueue;
    property CSLogDB: TCriticalSection read FCSLogDB write FCSLogDB;
    procedure Execute(); override;
    constructor Create(CreateSuspended: boolean);
    destructor Destroy(); override;
    function ISSended(OrderNO: string): boolean;
  end;

  TLoger = class(TObject)
  private
    FLogWriteDB: TLogWriteDB;
    FDBQueue: TQueue<TLogRecord>;
    FCSLogDB: TCriticalSection;
  public
    procedure AddLog(LogRec: TLogRecord); overload;
    procedure AddLog(EventName: string; EventDesp: string; Status: boolean;
      LogTime: TDateTime); overload;
    constructor Create(ConnStr: string; LogPort: string);
    destructor Destroy(); override;
    function ISSended(OrderNO: string): boolean;
  end;

procedure LogToFile(s: string);

implementation

{ TLog }

procedure LogToFile(s: string);
var
  ls: TStringList;
begin
  ls := TStringList.Create;
  if not FileExists('C:\celog.txt') then
    ls.SaveToFile('C:\celog.txt');
  ls.LoadFromFile('C:\celog.txt');
  ls.Add(s);
  ls.SaveToFile('C:\celog.txt');
end;

procedure TLoger.AddLog(LogRec: TLogRecord);
begin
  FCSLogDB.Acquire;
  FDBQueue.Enqueue(LogRec);
  FCSLogDB.Release;
end;

procedure TLoger.AddLog(EventName, EventDesp: string; Status: boolean;
  LogTime: TDateTime);
var
  r: TLogRecord;
begin
  r.EventName := EventName;
  r.MSG := EventDesp;
  if Status then
    r.Flag := 1
  else
    r.Flag := 0;
  r.LogTime := LogTime;
  r.CodeNum := 0;
  r.UserName := '';
  r.PayMoney := 0.0;
  r.YuanBao := 0.0;
  r.ServerIP := '';
  r.ClientIP := '';
  r.SendDate := now;
  r.OrderID := '';
  AddLog(r);
end;

constructor TLoger.Create(ConnStr: string; LogPort: string);
begin
  FCSLogDB := TCriticalSection.Create;
  FDBQueue := TQueue<TLogRecord>.Create;

  FLogWriteDB := TLogWriteDB.Create(True);
  FLogWriteDB.DBQueue := FDBQueue;

  FLogWriteDB.ConnStr := ConnStr;
  FLogWriteDB.CSLogDB := FCSLogDB;
  FLogWriteDB.Resume;
end;

destructor TLoger.Destroy;
begin
  FLogWriteDB.Terminate;
  FLogWriteDB.WaitFor;
  FLogWriteDB.Free;
  FCSLogDB.Free;
  FDBQueue.Free;

  inherited;
end;

function TLoger.ISSended(OrderNO: string): boolean;
begin
  result := FLogWriteDB.ISSended(OrderNO);
end;

{ TLogWriteDB }

constructor TLogWriteDB.Create(CreateSuspended: boolean);
begin
  inherited Create(CreateSuspended);
  FUC := TUniConnection.Create(nil);
  FUQ := TUniQuery.Create(nil);
  FUQ.Connection := FUC;
  FUQISSended := TUniQuery.Create(nil);
  FUQISSended.Connection := FUC;
end;

destructor TLogWriteDB.Destroy;
begin
  FUQ.Close;
  FUQISSended.Close;
  FUC.Close;
  FUQ.Free;
  FUQISSended.Free;
  FUC.Free;
  inherited;
end;

procedure TLogWriteDB.Execute;
var
  Rec: TLogRecord;
begin
  inherited;
  while (True) do
  begin
    if (Terminated) and (FDBQueue.Count = 0) then
      Break;
    if FDBQueue.Count = 0 then
    begin
      Sleep(100);
      continue;
    end;
    FCSLogDB.Acquire;
    Rec := FDBQueue.Dequeue;
    FCSLogDB.Release;

    WriteLog(Rec);
    Sleep(1);
  end;
end;

function TLogWriteDB.ISSended(OrderNO: string): boolean;
var
  sql: string;
begin
  sql := 'select ID from Log where OrderID=''' + (OrderNO) + ''' and Flag = 1 ';
  FUQISSended.Close;
  FUQISSended.sql.Text := sql;
  FUQISSended.Open;
  if FUQISSended.RecordCount > 0 then
    result := True
  else
    result := false;
  FUQISSended.Close;
end;

procedure TLogWriteDB.SetConnStr(const Value: string);
begin
  FConnStr := Value;
  ConnectDB(FUC, FConnStr);
end;

procedure TLogWriteDB.WriteLog(Rec: TLogRecord);
begin
  FUQ.Close;
  FUQ.sql.Clear;
  FUQ.sql.Text :=
    'Insert Into Log (CodeNum,UserName,PayMoney,YuanBao,ServerIP,ClientIP,Flag,SendDate,OrderID,message,EventName,LogTime) Values(:CodeNum,:UserName,:PayMoney, :YuanBao, :ServerIP, :ClientIP, :Flag,:SendDate,:OrderID,:MSG,:EventName,:LogTime)';
  FUQ.Params.ParamByName('CodeNum').Value := Rec.CodeNum;
  FUQ.Params.ParamByName('UserName').Value := Rec.UserName;
  FUQ.Params.ParamByName('PayMoney').Value := Rec.PayMoney;
  FUQ.Params.ParamByName('Flag').Value := Rec.Flag;
  FUQ.Params.ParamByName('SendDate').Value := Rec.SendDate;
  FUQ.Params.ParamByName('OrderID').Value := Rec.OrderID;
  FUQ.Params.ParamByName('MSG').Value := Rec.MSG;
  FUQ.Params.ParamByName('EventName').Value := Rec.EventName;
  FUQ.Params.ParamByName('LogTime').Value := Rec.LogTime;
  FUQ.Params.ParamByName('YuanBao').Value := Rec.YuanBao;
  FUQ.Params.ParamByName('ServerIP').Value := Rec.ServerIP;
  FUQ.Params.ParamByName('ClientIP').Value := Rec.ClientIP;
  FUQ.ExecSQL;
end;

end.
