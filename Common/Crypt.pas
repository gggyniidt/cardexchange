unit Crypt;

interface

uses Windows, SysUtils, Forms, Dialogs, classes, sockets, SyncObjs, adodb, DataDef, IdHashMessageDigest, IdHash;

procedure EN_DEXOR(const Buffer: TBytes; Offset, Len: Longint; const Key: AnsiString);
procedure EncyptCEPT(const CEPT: TCEPtl; var NetPck: TNetPck; Key: AnsiString);
function DecyptCEPT(const NetPck: TNetPck; var CEPT: TCEPtl; Key: AnsiString): Boolean;

function CheckData(Buffer: TBytes): Boolean;
function GenDataPack(CEP: TCEPtl): TBytes;

function MD5Str(s: String): String; overload;
function MD5Byte(buf: TBytes): TBytes; overload;

implementation

function GenDataPack(CEP: TCEPtl): TBytes;
var
  buf: TBytes;
  md5: TBytes;
  datasize: Integer;
begin
  datasize := SizeOf(TCEPtl);

  SetLength(buf, datasize);
  FillMemory(buf, datasize, 0);
  Move(CEP, buf[0], datasize);

  md5 := MD5Byte(buf);

  SetLength(buf, datasize + 16);
  Move(md5[0], buf[datasize], 16);

  Result := buf;
end;

function CheckData(Buffer: TBytes): Boolean;
var
  md5: TBytes;
  md52: TBytes;
  Data: TBytes;
  s: string;
  datasize: Integer;
begin
  datasize := SizeOf(TCEPtl);

  SetLength(Data, datasize);
  Move(Buffer[0], Data[0], datasize);

  SetLength(md5, 16);
  Move(Buffer[datasize], md5[0], 16);

  md52 := MD5Byte(Data);

  if CompareMem(md5, md52, 16) then
    Result := True
  else
    Result := False;
end;

function MD5Byte(buf: TBytes): TBytes;
var
  Md5Encode: TIdHashMessageDigest5;
begin
  Md5Encode := TIdHashMessageDigest5.Create;
  try
    Result := Md5Encode.HashBytes(buf);
  finally
    Md5Encode.Free;
  end;
end;

function MD5Str(s: String): String;
var
  Md5Encode: TIdHashMessageDigest5;
begin
  Md5Encode := TIdHashMessageDigest5.Create;
  try
    Result := Md5Encode.HashStringAsHex(s);
  finally
    Md5Encode.Free;
  end;
end;

procedure EN_DEXOR(const Buffer: TBytes; Offset, Len: Integer; const Key: AnsiString);
var
  BufIdx, KeyIdx: Longint;
begin
  KeyIdx := 1;
  for BufIdx := Offset to Offset + Len - 1 do
  begin
    if (BufIdx < Low(Buffer)) or (BufIdx > High(Buffer)) then
      Break;
    Buffer[BufIdx] := Buffer[BufIdx] xor Ord(Key[KeyIdx]);
    KeyIdx := KeyIdx mod Length(Key) + 1;
  end
end;

function DecyptCEPT(const NetPck: TNetPck; var CEPT: TCEPtl; Key: AnsiString): Boolean;
var
  buf: TBytes;
  sz: Integer;
  CHKSUM: TBytes;
begin
  sz := SizeOf(TCEPtl);
  SetLength(buf, sz);
  Move(NetPck.CEPTL, buf[0], sz);
  EN_DEXOR(buf, 0, sz, Key);
  Move(buf[0], CEPT, sz);
  CHKSUM := MD5Byte(buf);
  if CompareMem(CHKSUM, @NetPck.CHECKSUM, Length(NetPck.CHECKSUM)) then
    Result := True
  else
    Result := False;
  SetLength(buf, 0);
  SetLength(CHKSUM, 0);
end;

procedure EncyptCEPT(const CEPT: TCEPtl; var NetPck: TNetPck; Key: AnsiString);
var
  buf: TBytes;
  sz: Integer;
  CHKSUM: TBytes;
begin
  sz := SizeOf(TCEPtl);
  SetLength(buf, sz);
  Move(CEPT, buf[0], sz);
  CHKSUM := MD5Byte(buf);
  Move(CHKSUM[0], NetPck.CHECKSUM, Length(NetPck.CHECKSUM));
  EN_DEXOR(buf, 0, sz, Key);
  Move(buf[0], NetPck.CEPTL, sz);
  NetPck.CRYPTTYPE := ctXOR;
  SetLength(buf, 0);
  SetLength(CHKSUM, 0);
end;

end.
