unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, regware4, reggen4, Spin, ComCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Label1: TLabel;
    LicenseIDBox: TEdit;
    Label2: TLabel;
    RegCodeBox: TEdit;
    RegGen: TRegCodeGenerator4;
    procedure Button1Click(Sender: TObject);
  private
    function GetLicenseID: string;
    function GenerateCode: string;
  public
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

uses DataDef;

procedure TForm1.Button1Click(Sender: TObject);
begin
  LicenseIDBox.Text := GetLicenseID;
  RegCodeBox.Text := GenerateCode;
end;

function TForm1.GetLicenseID: string;
var
  i: integer;
  Str: string;
begin
  Randomize;
  for i := 0 to 9 do
    Str := Str + IntToStr(Random(10));
  Result := Str;
end;

function TForm1.GenerateCode: string;
var
  ExpireDate: TDateTime;
begin
  try
    Result := RegGen.GenerateCode(LicenseIDBox.Text, now + 15);
  except
    Result := '';
  end;
end;

end.
