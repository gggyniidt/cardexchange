object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 656
  ClientWidth = 771
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 352
    Top = 16
    Width = 75
    Height = 25
    Caption = 'LogTest'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 456
    Top = 128
    Width = 131
    Height = 25
    Caption = 'TCPClientWatcher'
    TabOrder = 1
    OnClick = Button2Click
  end
  object Memo1: TMemo
    Left = 8
    Top = 8
    Width = 249
    Height = 313
    Lines.Strings = (
      'Memo1')
    TabOrder = 2
  end
  object Button3: TButton
    Left = 472
    Top = 176
    Width = 75
    Height = 25
    Caption = 'DBScanner'
    TabOrder = 3
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 560
    Top = 176
    Width = 75
    Height = 25
    Caption = 'Button4'
    TabOrder = 4
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 288
    Top = 176
    Width = 75
    Height = 25
    Caption = 'Button5'
    TabOrder = 5
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 514
    Top = 207
    Width = 75
    Height = 25
    Caption = 'Button6'
    TabOrder = 6
    OnClick = Button6Click
  end
  object Button7: TButton
    Left = 369
    Top = 207
    Width = 75
    Height = 25
    Caption = 'Button7'
    TabOrder = 7
    OnClick = Button7Click
  end
  object Button8: TButton
    Left = 369
    Top = 238
    Width = 75
    Height = 25
    Caption = 'Button8'
    TabOrder = 8
    OnClick = Button8Click
  end
  object Button9: TButton
    Left = 512
    Top = 238
    Width = 75
    Height = 25
    Caption = 'Button9'
    TabOrder = 9
    OnClick = Button9Click
  end
  object Button10: TButton
    Left = 514
    Top = 16
    Width = 75
    Height = 25
    Caption = 'Button10'
    TabOrder = 10
    OnClick = Button10Click
  end
  object Button11: TButton
    Left = 433
    Top = 16
    Width = 75
    Height = 25
    Caption = 'Button11'
    TabOrder = 11
    OnClick = Button11Click
  end
  object Button12: TButton
    Left = 595
    Top = 16
    Width = 75
    Height = 25
    Caption = 'Button12'
    TabOrder = 12
    OnClick = Button12Click
  end
  object Button13: TButton
    Left = 528
    Top = 384
    Width = 75
    Height = 25
    Caption = 'Button13'
    TabOrder = 13
    OnClick = Button13Click
  end
  object Memo2: TMemo
    Left = 8
    Top = 360
    Width = 449
    Height = 241
    TabOrder = 14
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=emr_dtx;Data Source=192.168.1.102\ss_emr'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 432
    Top = 184
  end
  object TcpServer1: TTcpServer
    Active = True
    LocalHost = '127.0.0.1'
    LocalPort = '7878'
    OnAccept = TcpServer1Accept
    OnGetThread = TcpServer1GetThread
    Left = 288
    Top = 232
  end
  object TcpClient2: TTcpClient
    RemoteHost = '127.0.0.1'
    RemotePort = '1389'
    Left = 472
    Top = 56
  end
  object TcpClient1: TTcpClient
    BlockMode = bmNonBlocking
    RemoteHost = '127.0.0.1'
    RemotePort = '8888'
    OnError = TcpClient1Error
    Left = 320
    Top = 64
  end
  object UniConnection1: TUniConnection
    ProviderName = 'SQL Server'
    Database = 'test'
    Username = 'sa'
    Password = 'sa'
    Server = '192.168.1.110'
    LoginPrompt = False
    Left = 496
    Top = 328
  end
  object UniQuery1: TUniQuery
    Connection = UniConnection1
    Left = 544
    Top = 328
  end
  object SQLServerUniProvider1: TSQLServerUniProvider
    Left = 592
    Top = 328
  end
  object SimpleDataSet1: TSimpleDataSet
    Aggregates = <>
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 408
    Top = 288
  end
end
