unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, generics.collections, StdCtrls, Sockets, utils, DB, ADODB, DBScanner,
  UniProvider, SQLServerUniProvider, MemDS, DBAccess, Uni, DBClient, SimpleDS;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Memo1: TMemo;
    Button3: TButton;
    ADOConnection1: TADOConnection;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    TcpServer1: TTcpServer;
    Button8: TButton;
    Button9: TButton;
    TcpClient2: TTcpClient;
    Button10: TButton;
    Button11: TButton;
    Button12: TButton;
    TcpClient1: TTcpClient;
    Button13: TButton;
    Memo2: TMemo;
    UniConnection1: TUniConnection;
    UniQuery1: TUniQuery;
    SQLServerUniProvider1: TSQLServerUniProvider;
    SimpleDataSet1: TSimpleDataSet;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure TcpClient1Error(Sender: TObject; SocketError: Integer);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure TcpServer1Accept(Sender: TObject; ClientSocket: TCustomIpClient);
    procedure TcpServer1GetThread(Sender: TObject; var ClientSocketThread: TClientSocketThread);
    procedure Button8Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure Button10Click(Sender: TObject);
    procedure Button11Click(Sender: TObject);
    procedure Button12Click(Sender: TObject);
    procedure Button13Click(Sender: TObject);
  private
    { Private declarations }
  public
    function OnDBChange(var rec: TDBRecord): Boolean;
  end;

  TServerThread = class(TClientSocketThread)
  private
    s: string;
  protected
    procedure SyncProc; override;
    procedure GetAuthInfo(var pkg: TCEPtl);
    procedure SetMoney(pkg: TCEPtl);
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

uses global, Loger, Partitions;

type
  ttestthread = class(tthread)
  public
    // Log: TLog;
    procedure Execute; override;
  end;

var
  Log: TLoger;
  ds: TDBScanner;

procedure TForm1.Button10Click(Sender: TObject);
var
  r: TLogRecord;
begin
  TcpClient2.Open;
  while True do
  begin
    if TcpClient2.WaitForData then
    begin
      // r.PartitionID := TcpClient2.Receiveln();
      TcpClient2.ReceiveBuf(r, SizeOf(TLogRecord));
      // Memo1.Lines.Add(r.PartitionID + ':' + DateTimeToStr(r.LogTime));
    end;
    Application.ProcessMessages;
    Sleep(1);
  end;

end;

procedure TForm1.Button11Click(Sender: TObject);
var
  i: Integer;
  r: TLogRecord;
begin

  // r.PartitionID := IntToStr(i);
  // r.LogTime := Now;
  // Log.AddLog(r);

end;

procedure TForm1.Button12Click(Sender: TObject);
begin
  Log.Free;
  Sleep(1);
end;

procedure TForm1.Button13Click(Sender: TObject);
var
  ps: TPartitions;
  p: TPartition;
begin
  config.ConfigConnStr :=
    'ProviderName=Access;Username=;Password=;Server=;Port=0;Database=F:\CardExchange\bin\Client\Config.mdb';

  ps := TPartitions.Create;
  p := ps.Partition[003];
  p.ExecSQL('001', 200);
  // ShowMessage(p.PartitionInfo.SQLStr1);
  // UniQuery1.SQL.Text:=p.PartitionInfo.SQLStr1;
  // UniQuery1.Params.ParamByName('AccountValue').AsString:='001';
  // UniQuery1.Params.ParamByName('MoneyValue').AsInteger:=200;
  // UniQuery1.ExecSQL;
end;

procedure TForm1.Button1Click(Sender: TObject);
var
  i: Integer;
  r: TLogRecord;
begin
  LoadConfig;
  Log := TLoger.Create(config.ConfigConnStr, config.LogPort);

  // ds := TDBScanner.Create(config.ConnStr);
end;

{ ttestthread }

procedure ttestthread.Execute;
// var
// r: TLogRecord;
// i: Integer;
begin
  // inherited;
  // for i := 0 to 999 do
  // begin
  // // sleep(1);
  // r.PartitionID := IntToStr(Handle) + '_' + IntToStr(i);
  // Log.AddLog(r);
  // end;
end;

var
  TCPClientWatcher: TTCPClientWatcher;

procedure TForm1.Button2Click(Sender: TObject);
begin
  TCPClientWatcher := TTCPClientWatcher.Create(True);
  TCPClientWatcher.TCPClient := TcpClient1;
  TCPClientWatcher.Resume;
  TCPClientWatcher.RestartFlag := True;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  ds.TimeSpace := 1000;
  ds.OnDBChange := OnDBChange;
  ds.SQL := 'select * from MirMoUrl where flag=0  ORDER BY id DESC';
  ds.Resume;
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  ds.Terminate;
end;

procedure TForm1.Button5Click(Sender: TObject);
var
  r: TLogRecord;
  i:Integer;
begin
//  r.EventName := 'eventname';
//  r.MSG := 'eventmsg';
//  r.Flag := 0;
//  r.LogTime := now;
//  r.CodeNum := 0;
//  r.UserName := '';
//  r.PayMoney := 0;
//  r.SendDate :=  now;
//  r.OrderID := '';
//  Log.AddLog(r);
for I := 0 to 50- 1 do

  Log.AddLog('EventName', 'EventDesp', True, now);

end;

procedure TForm1.Button6Click(Sender: TObject);
begin
  TcpClient1.Sendln('quit')
end;

procedure TForm1.Button7Click(Sender: TObject);
var
  c: TCEPtl;
  Buf: TBytes;
  md5: TBytes;
  datasize: Integer;
begin
  // c.which := cewGetAuth;
  // // c.u.ran := 'ran';
  // c.u.PartitionID := 'PartitionID';
  // c.u.Account := 'Account';
  // c.u.Session := 'Session';
  // c.u.PasswordMD5 := 'PasswordMD5';

  Buf := GenDataPack(c);

  if CheckData(Buf) then
    ShowMessage('ok')
  else
    ShowMessage('no');

end;

procedure Encrypt(const Buffer: TBytes; Offset, Len: Longint; const Key: AnsiString);
var
  BufIdx, KeyIdx: Longint;
begin
  KeyIdx := 1;

  for BufIdx := Offset to Offset + Len - 1 do
  begin
    if (BufIdx < Low(Buffer)) or (BufIdx > High(Buffer)) then
      Break;

    Buffer[BufIdx] := Buffer[BufIdx] xor Ord(Key[KeyIdx]);

    KeyIdx := KeyIdx mod Length(Key) + 1
  end
end;

procedure Decrypt(const Buffer: TBytes; Offset, Len: Longint; const Key: AnsiString);
begin
  Encrypt(Buffer, Offset, Len, Key);
end;

procedure TForm1.Button8Click(Sender: TObject);
var
  x: Double;
begin
  Randomize;
  x := Random();
  ShowMessage(FloatToStr(x));
  Sleep(10);
end;

procedure TForm1.Button9Click(Sender: TObject);
var
  p: TCEPtl;
  dp: TBytes;
begin
  // p.which := cewGetAuth;
  // p.u.RanMD5 := '123';
  // dp := GenDataPack();

end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  // ConnStr :=
  // 'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ID=sa;Initial Catalog=emr_dtx;Data Source=192.168.1.102\ss_emr';
end;

function TForm1.OnDBChange(var rec: TDBRecord): Boolean;
var
  r: TLogRecord;
begin
  // r.PartitionID := 'FindRecord';
  // r.LogMsg := rec.orderId;
  // r.LogTime := Now;
  // Log.AddLog(r);
  rec.Flag := 1;
  Memo1.Lines.Add(rec.OrderID);
  Result := True;
end;

procedure TForm1.TcpClient1Error(Sender: TObject; SocketError: Integer);
begin
  TCPClientWatcher.RestartFlag := True;
  Memo1.Lines.Add(IntToStr(SocketError));

end;

procedure TForm1.TcpServer1Accept(Sender: TObject; ClientSocket: TCustomIpClient);
var
  t: TServerThread;
  pkg: TCEPtl;
  RcvBuf, SndBuf: TBytes;
  datasize: Integer;
  c: Integer;
begin
  t := TServerThread(ClientSocket.GetThreadObject);
  t.s := ClientSocket.RemoteHost + '已连接';
  t.SyncProc;

  datasize := SizeOf(TCEPtl) + 16;
  SetLength(RcvBuf, datasize);

  while not t.Terminated and ClientSocket.Connected do
  begin
    if ClientSocket.WaitForData(0) then
    begin
      FillMemory(RcvBuf, datasize, 0);
      c := ClientSocket.ReceiveBuf(RcvBuf[0], datasize);
      if c <= 0 then
        Continue;

      if CheckData(RcvBuf) = False then
      begin
        pkg.which := cewError;
        // pkg.u.ErrorMessage := '数据包验证错误';
        SndBuf := GenDataPack(pkg);
        ClientSocket.SendBuf(SndBuf[0], datasize);
        SetLength(SndBuf, 0);
        Continue;
      end;

      CopyMemory(@pkg, RcvBuf, SizeOf(TCEPtl));

      case pkg.which of
        cewData:
          begin
            t.GetAuthInfo(pkg);
            SndBuf := GenDataPack(pkg);
            ClientSocket.SendBuf(SndBuf[0], datasize);
            SetLength(SndBuf, 0);
            Continue;
          end;
        cewError:
          begin
            t.SetMoney(pkg);
            pkg.which := cewSucc;
            // pkg.u.Msg := '操作成功';
            SndBuf := GenDataPack(pkg);
            ClientSocket.SendBuf(SndBuf[0], datasize);
            SetLength(SndBuf, 0);
            Continue;
          end;
        cewSucc:
          begin
            ClientSocket.Disconnect;
            Continue;
          end;
      end;
    end;
    Sleep(1);
  end;
  t.s := ClientSocket.RemoteHost + '已断开';
  t.SyncProc
end;

procedure TForm1.TcpServer1GetThread(Sender: TObject; var ClientSocketThread: TClientSocketThread);
begin
  ClientSocketThread := TServerThread.Create(TTcpServer(Sender).ServerSocketThread);
end;

{ TServerThread }

procedure TServerThread.GetAuthInfo(var pkg: TCEPtl);
var
  s: string;
begin
  // pkg.which := cewRetAuth;
  // pkg.u.PasswordMD5 := 'test';

end;

procedure TServerThread.SetMoney(pkg: TCEPtl);
begin
  //
end;

procedure TServerThread.SyncProc;
begin
  inherited;

end;

end.
