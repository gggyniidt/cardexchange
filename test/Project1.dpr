program Project1;

uses
  Forms,
  Unit1 in 'Unit1.pas' {Form1},
  Utils in '..\common\Utils.pas',
  global in '..\Server\global.pas',
  DBScanner in '..\Server\Server\DBScanner.pas',
  Partitions in '..\Client\Partitions.pas',
  Loger in '..\Common\Loger.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
