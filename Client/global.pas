unit global;

interface

uses Windows, SysUtils, ShellAPI, Forms, Dialogs, classes, sockets, generics.collections, SyncObjs, adodb, UniDacVcl,
  Uni, Winsock, Partitions, UniProvider, ODBCUniProvider, AccessUniProvider;

const
  ServiceName = 'Svc_CardExchangeClient';
  ServiceAppName = 'CardExchangeClient.exe';

type
  TConfig = record
    ServicePort: string;
    ServicePassword: string;
    LogPort: string;
    ServerIP: string;
    FixedServer: Boolean;
    ConfigConnStr: string;
  end;

function SaveConfig: Boolean;
function LoadConfig: Boolean;

// function GetPartitionInfo(UQ: TUniQuery; PartitionID: Integer): TPartitionInfo;
function GenSQL1: string;
function GenSQL2: string;

var
  Partitions: TPartitions;
  config: TConfig;

implementation

uses Utils;

function GenSQL1: string;
begin

end;

function GenSQL2: string;
begin

end;

function GetPartitionInfo(UQ: TUniQuery; PartitionID: Integer): TPartitionInfo;
var
  PI: TPartitionInfo;
begin
  UQ.Close;
  UQ.SQL.Text := 'Select * from Partition Where PartitionID=' + QuotedStr(inttostr(PartitionID));
  UQ.Open;

  if UQ.RecordCount = 0 then
  begin
    result.PartitionID := '-1';
    Exit;
  end;

  with PI, UQ do
  begin
    Proportion := FieldByName('Proportion').AsInteger;
    DBIndex := FieldByName('DBIndex').AsInteger;

    case DBIndex of
      0:
        begin
          DBConn1 := FieldByName('DBConn1').AsString;
          TableName1 := FieldByName('TableName1').AsString;
          AccountField := FieldByName('AccountField').AsString;
          MoneyField := FieldByName('MoneyField').AsString;
        end;
      1:
        begin
          DBConn1 := FieldByName('DBConn1').AsString;
          TableName1 := FieldByName('TableName1').AsString;
          AccountField := FieldByName('AccountField').AsString;
          IDField1 := FieldByName('IDField1').AsString;

          DBConn2 := FieldByName('DBConn2').AsString;
          TableName2 := FieldByName('TableName2').AsString;
          MoneyField := FieldByName('MoneyField').AsString;
          IDField2 := FieldByName('IDField2').AsString;
        end;
      2:
        begin
          DBConn1 := FieldByName('DBConn1').AsString;
          SQLStr1 := FieldByName('SQLStr').AsString;
        end;
    end;
    Close;
  end;
  result := PI;
end;

function SaveConfig: Boolean;
var
  UC: TUniConnection;
  UT: TUniTable;
begin
  UC := TUniConnection.Create(nil);
  UT := TUniTable.Create(nil);
  try
    try
      ConnectDB(UC, config.ConfigConnStr);
      UT.Connection := UC;
      UT.TableName := 'ServiceConfig';
      UT.Open;
      UT.Edit;
      UT.FieldByName('ServicePort').AsString := config.ServicePort;
      UT.FieldByName('LogPort').AsString := config.LogPort;
      UT.FieldByName('ServicePassword').AsString := config.ServicePassword;
      UT.FieldByName('FixedServer').AsBoolean := config.FixedServer;
      UT.FieldByName('ServerIP').AsString := config.ServerIP;
      UT.Post;
      UT.Close;
      UC.Close;
      result := True;
    except
      result := False;
    end;
  finally
    UT.Free;
    UC.Free
  end;
end;

function LoadConfig: Boolean;
var
  UC: TUniConnection;
  UT: TUniTable;
begin
  UC := TUniConnection.Create(nil);
  UT := TUniTable.Create(nil);
  try
    try
      ConnectDB(UC, config.ConfigConnStr);
      UT.Connection := UC;
      UT.TableName := 'ServiceConfig';
      UT.Open;
      config.ServicePort := UT.FieldByName('ServicePort').AsString;
      config.LogPort := UT.FieldByName('LogPort').AsString;
      config.ServicePassword := UT.FieldByName('ServicePassword').AsString;
      config.FixedServer := UT.FieldByName('FixedServer').AsBoolean;
      config.ServerIP := UT.FieldByName('ServerIP').AsString;
      UT.Close;
      UC.Close;
      result := True;
    except
      result := False;
      Log('loadexce');
    end;
  finally
    UT.Free;
    UC.Free;
  end;
end;

initialization

config.ConfigConnStr := 'ProviderName=Access;Username=;Password=;Server=;Port=0;Database=' + AppPath + 'config.mdb';

finalization

end.
