unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ShellAPI, winsvc, ComCtrls, Mask, ExtCtrls, Menus, ADODB,
  Sockets, DB, IdBaseComponent, IdAntiFreezeBase, IdAntiFreeze,
  generics.Collections, Grids, DBGrids, DBCtrls, ToolWin, ImgList, RzButton,
  RzPanel, WideStrings, DBXMsSQL, SqlExpr, RzEdit, DBAccess, Uni, RzLabel,
  RzStatus, MemDS, SQLServerUniProvider, MySQLUniProvider, UniProvider,
  ODBCUniProvider, AccessUniProvider, DateUtils, Math,
  DBClient, RzGrids, RzDBGrid;

type
  TFrm_Main = class(TForm)
    Button4: TButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    TrayIcon: TTrayIcon;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    TabSheet3: TTabSheet;
    DBGrid1: TDBGrid;
    ImageList1: TImageList;
    Label4: TLabel;
    Edit3: TEdit;
    Edit4: TEdit;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Button5: TButton;
    RzNumericEdit1: TRzNumericEdit;
    UniDataSource1: TUniDataSource;
    UT_Partition: TUniTable;
    UC_Config: TUniConnection;
    AccessUniProvider1: TAccessUniProvider;
    ODBCUniProvider1: TODBCUniProvider;
    MySQLUniProvider1: TMySQLUniProvider;
    SQLServerUniProvider1: TSQLServerUniProvider;
    CheckBox1: TCheckBox;
    Edit1: TEdit;
    UniConnection1: TUniConnection;
    UniQuery1: TUniQuery;
    UniDataSource2: TUniDataSource;
    DBGrid2: TDBGrid;
    RzToolbar2: TRzToolbar;
    Btn_Install: TRzToolButton;
    Btn_Start: TRzToolButton;
    Label7: TLabel;
    RzToolButton4: TRzToolButton;
    RzToolButton5: TRzToolButton;
    RzSpacer1: TRzSpacer;
    Label8: TLabel;
    Label10: TLabel;
    DateTimePicker1: TDateTimePicker;
    DateTimePicker2: TDateTimePicker;
    Edit2: TEdit;
    CheckBox2: TCheckBox;
    RzToolbar1: TRzToolbar;
    BtnInsertRecord: TRzToolButton;
    BtnDeleteRecord: TRzToolButton;
    BtnEditRecord: TRzToolButton;
    BtnRefresh: TRzToolButton;
    RzSpacer2: TRzSpacer;
    Label2: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    RzStatusBar2: TRzStatusBar;
    RzSpacer4: TRzSpacer;
    RzSpacer5: TRzSpacer;
    RzSpacer6: TRzSpacer;
    UniQuery1ID: TIntegerField;
    UniQuery1CodeNum: TIntegerField;
    UniQuery1UserName: TWideStringField;
    UniQuery1payMoney: TFloatField;
    UniQuery1Flag: TIntegerField;
    UniQuery1SendDate: TDateTimeField;
    UniQuery1OrderID: TWideStringField;
    UniQuery1Message: TWideStringField;
    UniQuery1EventName: TWideStringField;
    UniQuery1LogTime: TDateTimeField;
    RzSpacer3: TRzSpacer;
    Label3: TLabel;
    UniQuery1YuanBao: TFloatField;
    Button1: TButton;
    procedure Button4Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TrayIconDblClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure N2Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure Edit4Change(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Btn_InstallClick(Sender: TObject);
    procedure Btn_StartClick(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
    procedure DateTimePicker1Change(Sender: TObject);
    procedure Edit2Change(Sender: TObject);
    procedure RzToolButton4Click(Sender: TObject);
    procedure RzToolButton5Click(Sender: TObject);
    procedure BtnInsertRecordClick(Sender: TObject);
    procedure BtnDeleteRecordClick(Sender: TObject);
    procedure BtnEditRecordClick(Sender: TObject);
    procedure BtnRefreshClick(Sender: TObject);
    procedure DateTimePicker2Change(Sender: TObject);
    procedure UniQuery1FlagGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure Button1Click(Sender: TObject);
  private
    procedure ButtonUpdate();
    procedure UpdateConfig();
    procedure InitLog();
    procedure InitPartition();
    procedure query;
  public
  end;

var
  Frm_Main: TFrm_Main;

implementation

{$R *.dfm}

uses partion, Partitions, Utils, global, loger;

{ DBGridAutoSize }
procedure TFrm_Main.BtnDeleteRecordClick(Sender: TObject);
begin
  if UT_Partition.RecordCount = 0 then
    Exit;
  case MessageDlg('确实要删除此分区吗?', mtConfirmation, mbYesNo, 0) of
    mrYes:
      UT_Partition.Delete;
    mrNo:
      ;
  end;
end;

procedure TFrm_Main.BtnEditRecordClick(Sender: TObject);
begin
  Frm_Partion.UT.Close;
  Frm_Partion.UT.Open;
  Frm_Partion.LoadData(UT_Partition.FieldByName('ID').AsInteger);
  Frm_Partion.UT.Edit;
  Frm_Partion.Parent := TabSheet3;
  RzToolbar1.Visible := False;
  Frm_Partion.SetEnable(True);
  Frm_Partion.Show;
end;

procedure TFrm_Main.BtnInsertRecordClick(Sender: TObject);
begin
  Frm_Partion.UT.Close;
  Frm_Partion.UT.Open;
  Frm_Partion.UT.Append;
  Frm_Partion.DataClear;
  Frm_Partion.Parent := TabSheet3;
  RzToolbar1.Visible := False;
  Frm_Partion.SetEnable(True);
  Frm_Partion.Show;
end;

procedure TFrm_Main.BtnRefreshClick(Sender: TObject);
begin
  UT_Partition.Close;
  UT_Partition.Open;
end;

procedure TFrm_Main.Btn_InstallClick(Sender: TObject);
begin
  if ServiceUninstalled(ServiceName) then // 未安装
    ExecnWait(ServiceAppName, ' /install')
  else
    ExecnWait(ServiceAppName, ' /uninstall'); // 已安装

  ButtonUpdate;
end;

procedure TFrm_Main.Btn_StartClick(Sender: TObject);
begin
  if ServiceRunning(ServiceName) then
    ExecnWait('net', 'stop ' + ServiceName)
  else
    ExecnWait('net', 'start ' + ServiceName);

  ButtonUpdate;
end;

procedure TFrm_Main.Button1Click(Sender: TObject);
var
  emsg: string;
  rst: Boolean;
  FLoger: TLoger;
  logrec: TLogRecord;
begin
  FLoger := TLoger.Create(config.ConfigConnStr, config.LogPort);
  logrec.PayMoney := StrToCurr(RzNumericEdit1.Text);
  logrec.YuanBao := 100;
  logrec.LogTime := Now;
  logrec.SendDate := Now;
  logrec.EventName := '测试';
  rst := True;
  if rst then
  begin
    logrec.Flag := 1;
    logrec.MSG := '不计入结算';
    ShowMessage('充值成功');
  end
  else
  begin
    logrec.Flag := 0;
    if emsg = '' then
      emsg := '请检查玩家帐号是否正确';
    logrec.MSG := emsg;
    ShowMessage('充值失败:' + emsg);
  end;
  FLoger.AddLog(logrec);
  FLoger.Free;
end;


procedure TFrm_Main.Button4Click(Sender: TObject);
begin
  config.ServicePort := RzNumericEdit1.Text;
  // config.LogPort := RzNumericEdit2.Text;
  config.ServicePassword := Edit3.Text;
  config.FixedServer := CheckBox1.Checked;
  config.ServerIP := Edit1.Text;
  SaveConfig();
  ShowMessage('请注意，本次修改需在重启服务后生效，只需点击网关上的“停止/启动”按钮即可');
end;

procedure TFrm_Main.Button5Click(Sender: TObject);
begin
  if IsPortUsed(StrToInt(RzNumericEdit1.Text)) then
    ShowMessage('端口已占用')
  else
    ShowMessage('端口可以使用');
end;

procedure TFrm_Main.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caNone;
  Hide;
end;

procedure TFrm_Main.FormCreate(Sender: TObject);
begin

  LoadConfig;
  InitLog;
  InitPartition;
end;

procedure TFrm_Main.FormShow(Sender: TObject);
begin
  UpdateConfig;
  ButtonUpdate;

  DateTimePicker1.Date := StrToDate(DateToStr(Now));
  DateTimePicker1.Time := StrToTime('0:00:00');
  DateTimePicker2.Date := DateTimePicker1.Date;
  DateTimePicker2.Time := StrToTime('0:00:00');
  query;
end;

procedure TFrm_Main.InitLog;
begin
  ConnectDB(UniConnection1, config.ConfigConnStr);
  query;
end;

procedure TFrm_Main.query;
begin
  UniQuery1.Close;
  if CheckBox2.Checked then
    UniQuery1.SQL.Text := 'select * from log where logtime>=#' + DateToStr
      (DateTimePicker1.Date) + '# and logtime<=#' + DateToStr
      (IncDay(DateTimePicker2.Date)) + '# order by logtime desc'
  else
    UniQuery1.SQL.Text := 'select * from log order by logtime desc';
  UniQuery1.Open;
end;

procedure TFrm_Main.RzToolButton4Click(Sender: TObject);
begin
  query;
end;

procedure TFrm_Main.RzToolButton5Click(Sender: TObject);
var
  row: Integer;
begin
  for row := 0 to UniQuery1.RecordCount - 1 do
    UniQuery1.Delete;
end;

procedure TFrm_Main.InitPartition;
begin
  ConnectDB(UC_Config, config.ConfigConnStr);
  UT_Partition.Open;
end;

procedure TFrm_Main.N1Click(Sender: TObject);
begin
  Show;
end;

procedure TFrm_Main.N2Click(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TFrm_Main.PageControl1Change(Sender: TObject);
begin
  // if PageControl1.ActivePageIndex = 0 then
  // Width := 500
  // else if PageControl1.ActivePageIndex = 1 then
  // Width := 500
  // else if PageControl1.ActivePageIndex = 2 then
  // Width := 650;
end;

procedure TFrm_Main.Timer1Timer(Sender: TObject);
begin
  query;
end;

procedure TFrm_Main.TrayIconDblClick(Sender: TObject);
begin
  Show;
end;

procedure TFrm_Main.UniQuery1FlagGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  if Sender.FieldName = 'Flag' then
    DisplayText := False;
end;

procedure TFrm_Main.UpdateConfig;
begin
  RzNumericEdit1.Text := config.ServicePort;
  // RzNumericEdit2.Text := config.LogPort;
  Edit3.Text := config.ServicePassword;
  Edit1.Text := config.ServerIP;
  CheckBox1.Checked := config.FixedServer;
  if CheckBox1.Checked then
    Edit1.Enabled := True
  else
    Edit1.Enabled := False;
end;

procedure TFrm_Main.ButtonUpdate;
begin
  if ServiceUninstalled(ServiceName) then
  // 未安装
  begin
    Btn_Install.Caption := '安装服务';
    Btn_Start.Enabled := False;
  end
  else
  begin
    Btn_Install.Caption := '卸载服务'; // 已安装
    Btn_Start.Enabled := True;
  end;

  if ServiceRunning(ServiceName) then // 运行中
    Btn_Start.Caption := '停止'
  else
    Btn_Start.Caption := '启动'; // 停止
end;

procedure TFrm_Main.CheckBox1Click(Sender: TObject);
begin
  Edit1.Enabled := CheckBox1.Checked;
end;

procedure TFrm_Main.CheckBox2Click(Sender: TObject);
begin
  if CheckBox2.Checked then
  begin
    DateTimePicker1.Enabled := True;
    DateTimePicker2.Enabled := True;
  end
  else
  begin
    DateTimePicker1.Enabled := False;
    DateTimePicker2.Enabled := False;
  end;
  query;
end;

procedure TFrm_Main.DateTimePicker1Change(Sender: TObject);
begin
  query;
end;

procedure TFrm_Main.DateTimePicker2Change(Sender: TObject);
begin
  query;
end;

procedure TFrm_Main.DBGrid1DblClick(Sender: TObject);
begin
  Frm_Partion.UT.Close;
  Frm_Partion.UT.Open;
  Frm_Partion.LoadData(UT_Partition.FieldByName('ID').AsInteger);
  Frm_Partion.Parent := TabSheet3;
  RzToolbar1.Visible := False;
  Frm_Partion.SetEnable(False);
  Frm_Partion.Show;
end;

procedure TFrm_Main.DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  gd: TDBGrid;
begin
  if DataCol <> 3 then
    Exit;
  if gd.DataSource.DataSet.RecordCount <= 0 then
    Exit;
  gd := Sender as TDBGrid;

  if gd.DataSource.DataSet.FieldByName('Flag').AsInteger = 1 then
    gd.Canvas.TextOut(Rect.Left, Rect.Top, '成功')
  else
    gd.Canvas.TextOut(Rect.Left, Rect.Top, '失败')
end;

procedure TFrm_Main.Edit2Change(Sender: TObject);
begin
  if Edit2.Text = '' then
  begin
    UniQuery1.Filter := '';
    UniQuery1.Filtered := False;
  end
  else
  begin
    UniQuery1.Filter := 'OrderID Like ' + QuotedStr('%' + Edit2.Text + '%')
      + ' or UserName Like ' + QuotedStr('%' + Edit2.Text + '%')
      + ' or EventName Like ' + QuotedStr('%' + Edit2.Text + '%');
    UniQuery1.Filtered := True;
  end;
end;

procedure TFrm_Main.Edit4Change(Sender: TObject);
begin
  if Edit4.Text = '' then
  begin
    UT_Partition.Filter := '';
    UT_Partition.Filtered := False;
  end
  else
  begin
    UT_Partition.Filter := 'PartitionID Like ' + QuotedStr
      ('%' + Edit4.Text + '%') + ' or ServerName Like ' + QuotedStr
      ('%' + Edit4.Text + '%');
    UT_Partition.Filtered := True;
  end;
end;

end.
