object Frm_Partion: TFrm_Partion
  Left = 0
  Top = 0
  Align = alClient
  BorderStyle = bsNone
  Caption = 'Frm_Partion'
  ClientHeight = 416
  ClientWidth = 677
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label21: TLabel
    Left = 483
    Top = 8
    Width = 48
    Height = 13
    Caption = #36873#25321#27169#29256
  end
  object Shape1: TShape
    Left = 479
    Top = 8
    Width = 2
    Height = 400
  end
  object Button1: TButton
    Left = 308
    Top = 384
    Width = 75
    Height = 25
    Caption = #21462#28040
    ModalResult = 2
    TabOrder = 5
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 389
    Top = 384
    Width = 75
    Height = 25
    Caption = #30830#23450
    ModalResult = 1
    TabOrder = 6
    OnClick = Button2Click
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 459
    Height = 89
    Caption = #20998#21306#35774#32622
    TabOrder = 0
    object Label5: TLabel
      Left = 13
      Top = 24
      Width = 35
      Height = 13
      Caption = #20998#21306'ID'
    end
    object Label6: TLabel
      Left = 207
      Top = 24
      Width = 60
      Height = 13
      Caption = #26381#21153#22120#21517#31216
    end
    object Label7: TLabel
      Left = 13
      Top = 55
      Width = 148
      Height = 13
      Caption = #20805#20540#27604#20363' 1:'#12288#12288#12288#12288#12288' '#20803#23453
    end
    object Edit1: TEdit
      Left = 54
      Top = 21
      Width = 147
      Height = 21
      TabOrder = 0
    end
    object Edit2: TEdit
      Left = 273
      Top = 21
      Width = 172
      Height = 21
      TabOrder = 1
    end
    object RzNumericEdit2: TRzNumericEdit
      Left = 78
      Top = 52
      Width = 57
      Height = 21
      TabOrder = 2
      DisplayFormat = '0;(0)'
    end
  end
  object PageControl1: TPageControl
    Left = 8
    Top = 144
    Width = 469
    Height = 161
    ActivePage = TabSheet6
    TabOrder = 3
    object TabSheet4: TTabSheet
      Caption = 'TabSheet4'
      TabVisible = False
      ExplicitWidth = 455
      object GroupBox4: TGroupBox
        Left = 3
        Top = 3
        Width = 462
        Height = 102
        Caption = #25968#25454#24211
        TabOrder = 0
        object Label1: TLabel
          Left = 4
          Top = 19
          Width = 48
          Height = 13
          Caption = #25968#25454#24211#21517
        end
        object Label2: TLabel
          Left = 4
          Top = 46
          Width = 48
          Height = 13
          Caption = #24080#21495#23383#27573
        end
        object Label3: TLabel
          Left = 233
          Top = 19
          Width = 48
          Height = 13
          Caption = #24080#21495#34920#21517
        end
        object Label4: TLabel
          Left = 233
          Top = 46
          Width = 48
          Height = 13
          Caption = #28857#25968#23383#27573
        end
        object Edit4: TEdit
          Left = 58
          Top = 16
          Width = 136
          Height = 21
          ReadOnly = True
          TabOrder = 1
        end
        object Edit5: TEdit
          Left = 293
          Top = 16
          Width = 153
          Height = 21
          TabOrder = 2
        end
        object Edit6: TEdit
          Left = 58
          Top = 43
          Width = 169
          Height = 21
          TabOrder = 3
        end
        object Edit7: TEdit
          Left = 293
          Top = 43
          Width = 153
          Height = 21
          TabOrder = 4
        end
        object Button4: TButton
          Left = 200
          Top = 14
          Width = 27
          Height = 25
          Caption = '...'
          TabOrder = 0
          OnClick = Button4Click
        end
        object CheckBox2: TCheckBox
          Left = 3
          Top = 70
          Width = 310
          Height = 17
          Caption = #26356#26032#25968#25454#19981#23384#22312#26102#20801#35768#33258#21160#25554#20837
          TabOrder = 5
        end
      end
    end
    object TabSheet5: TTabSheet
      Caption = 'TabSheet5'
      ImageIndex = 1
      TabVisible = False
      ExplicitWidth = 455
      object GroupBox2: TGroupBox
        Left = 3
        Top = 3
        Width = 462
        Height = 74
        Caption = #25968#25454#24211'1'
        TabOrder = 0
        object Label8: TLabel
          Left = 4
          Top = 19
          Width = 48
          Height = 13
          Caption = #25968#25454#24211#21517
        end
        object Label9: TLabel
          Left = 4
          Top = 46
          Width = 48
          Height = 13
          Caption = #24080#21495#23383#27573
        end
        object Label10: TLabel
          Left = 233
          Top = 19
          Width = 48
          Height = 13
          Caption = #24080#21495#34920#21517
        end
        object Label11: TLabel
          Left = 233
          Top = 46
          Width = 35
          Height = 13
          Caption = 'ID'#23383#27573
        end
        object Edit8: TEdit
          Left = 58
          Top = 16
          Width = 136
          Height = 21
          ReadOnly = True
          TabOrder = 1
        end
        object Edit9: TEdit
          Left = 287
          Top = 16
          Width = 151
          Height = 21
          TabOrder = 2
        end
        object Edit10: TEdit
          Left = 58
          Top = 43
          Width = 169
          Height = 21
          TabOrder = 3
        end
        object Edit11: TEdit
          Left = 287
          Top = 43
          Width = 151
          Height = 21
          TabOrder = 4
        end
        object Button5: TButton
          Left = 200
          Top = 14
          Width = 27
          Height = 25
          Caption = '...'
          TabOrder = 0
          OnClick = Button5Click
        end
      end
      object GroupBox3: TGroupBox
        Left = 3
        Top = 73
        Width = 462
        Height = 74
        Caption = #25968#25454#24211'2'
        TabOrder = 1
        object Label12: TLabel
          Left = 4
          Top = 19
          Width = 48
          Height = 13
          Caption = #25968#25454#24211#21517
        end
        object Label13: TLabel
          Left = 4
          Top = 46
          Width = 48
          Height = 13
          Caption = #28857#25968#23383#27573
        end
        object Label14: TLabel
          Left = 233
          Top = 19
          Width = 48
          Height = 13
          Caption = #28857#25968#34920#21517
        end
        object Label15: TLabel
          Left = 233
          Top = 46
          Width = 35
          Height = 13
          Caption = 'ID'#23383#27573
        end
        object Edit12: TEdit
          Left = 58
          Top = 16
          Width = 136
          Height = 21
          ReadOnly = True
          TabOrder = 1
        end
        object Edit13: TEdit
          Left = 287
          Top = 16
          Width = 151
          Height = 21
          TabOrder = 2
        end
        object Edit14: TEdit
          Left = 58
          Top = 43
          Width = 169
          Height = 21
          TabOrder = 3
        end
        object Edit15: TEdit
          Left = 287
          Top = 43
          Width = 151
          Height = 21
          TabOrder = 4
        end
        object Button6: TButton
          Left = 200
          Top = 15
          Width = 27
          Height = 25
          Caption = '...'
          TabOrder = 0
          OnClick = Button6Click
        end
      end
    end
    object TabSheet6: TTabSheet
      Caption = 'TabSheet6'
      ImageIndex = 2
      TabVisible = False
      ExplicitWidth = 455
      object Label19: TLabel
        Left = 216
        Top = 72
        Width = 37
        Height = 13
        Caption = 'Label19'
      end
      object Label20: TLabel
        Left = 0
        Top = 138
        Width = 461
        Height = 13
        Align = alBottom
        Caption = 
          #31034#20363#65306'update account set [orderno]=:orderno, [money]=[money]+:money' +
          ' where [username]=:username'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ExplicitWidth = 517
      end
      object Memo1: TMemo
        Left = 0
        Top = 30
        Width = 461
        Height = 108
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        Lines.Strings = (
          'If Exists(Select * From ['#24080#21495#34920#21517'] Where ['#24080#21495#23383#27573'] = :AccountValue)'
          
            '    Update ['#24080#21495#34920#21517'] Set ['#35746#21333#21495#23383#27573'] = :OrderNoValue, ['#28857#25968#23383#27573'] = ['#28857#25968#23383#27573'] +' +
            ' :MoneyValue Where ['#24080#21495#23383#27573']= :AccountValue'
          'else'
          
            '    Insert Into ['#24080#21495#34920#21517'] (['#35746#21333#21495#23383#27573'], ['#24080#21495#23383#27573'], ['#28857#25968#23383#27573']) Values (:OrderN' +
            'oValue, :AccountValue, :MoneyValue)')
        ParentFont = False
        ScrollBars = ssBoth
        TabOrder = 1
        ExplicitWidth = 455
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 461
        Height = 30
        Align = alTop
        Caption = 'Panel1'
        TabOrder = 0
        ExplicitWidth = 455
        object Label18: TLabel
          Left = 9
          Top = 5
          Width = 36
          Height = 13
          Caption = #25968#25454#24211
        end
        object Button7: TButton
          Left = 426
          Top = 2
          Width = 24
          Height = 25
          Caption = '...'
          TabOrder = 0
          OnClick = Button7Click
        end
        object Edit3: TEdit
          Left = 51
          Top = 5
          Width = 369
          Height = 21
          ReadOnly = True
          TabOrder = 1
        end
      end
    end
  end
  object RadioGroup1: TRadioGroup
    Left = 8
    Top = 105
    Width = 234
    Height = 42
    Caption = #25968#25454#24211#37197#32622
    Columns = 3
    ItemIndex = 0
    Items.Strings = (
      #21333#24211#21333#34920
      #22810#24211#22810#34920
      #33258#23450#20041)
    TabOrder = 1
    OnClick = RadioGroup1Click
  end
  object GroupBox5: TGroupBox
    Left = 8
    Top = 311
    Width = 463
    Height = 47
    Caption = #20805#20540#27979#35797
    TabOrder = 4
    object Label16: TLabel
      Left = 13
      Top = 20
      Width = 48
      Height = 13
      Caption = #24080#25143#21517#31216
    end
    object Label17: TLabel
      Left = 194
      Top = 20
      Width = 24
      Height = 13
      Caption = #37329#39069
    end
    object Edit16: TEdit
      Left = 67
      Top = 16
      Width = 121
      Height = 21
      TabOrder = 0
    end
    object RzNumericEdit1: TRzNumericEdit
      Left = 240
      Top = 16
      Width = 65
      Height = 21
      TabOrder = 1
      DisplayFormat = '0;(0)'
    end
    object Button3: TButton
      Left = 328
      Top = 14
      Width = 75
      Height = 25
      Caption = #20805#20540
      TabOrder = 2
      OnClick = Button3Click
    end
  end
  object GroupBox6: TGroupBox
    Left = 248
    Top = 106
    Width = 223
    Height = 41
    Caption = #39640#32423#36873#39033
    TabOrder = 2
    object CheckBox1: TCheckBox
      Left = 3
      Top = 15
      Width = 118
      Height = 17
      Caption = #25903#25345'utf8'#23383#31526#38598
      TabOrder = 0
    end
  end
  object DBGrid1: TDBGrid
    Left = 483
    Top = 27
    Width = 187
    Height = 331
    DataSource = DataSource1
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    ReadOnly = True
    TabOrder = 7
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'ServerName'
        Title.Caption = #27169#29256#21517#31216
        Visible = True
      end>
  end
  object Button9: TButton
    Left = 485
    Top = 383
    Width = 75
    Height = 25
    Caption = #20445#23384#27169#29256
    TabOrder = 8
    OnClick = Button9Click
  end
  object Button10: TButton
    Left = 595
    Top = 383
    Width = 75
    Height = 25
    Caption = #21024#38500#27169#29256
    TabOrder = 9
    OnClick = Button10Click
  end
  object UT: TUniTable
    TableName = 'Partition'
    Connection = Frm_Main.UC_Config
    Left = 16
    Top = 352
  end
  object UT2: TUniTable
    TableName = 'Templete'
    Connection = Frm_Main.UC_Config
    Left = 536
    Top = 192
  end
  object DataSource1: TDataSource
    DataSet = UT2
    Left = 536
    Top = 240
  end
end
