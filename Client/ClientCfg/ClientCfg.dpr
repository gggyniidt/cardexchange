program ClientCfg;

uses
  Forms, Windows, Dialogs,
  Main in 'Main.pas' { Frm_Main } ,
  Partion in 'Partion.pas' { Frm_Partion } ,
  Utils in '..\..\Common\Utils.pas',
  global in '..\global.pas',
  Partitions in '..\Partitions.pas',
  Loger in '..\..\Common\Loger.pas';
{$R *.res}

VAR
  Mutex: THandle;

begin
  Mutex := CreateMutex(NIL, True, '{6A692D64-432E-4198-9911-D39F6F7A34C7}');

  IF GetLastError <> ERROR_ALREADY_EXISTS THEN // 如果不存在另一实例
  begin
    Application.Initialize;
    Application.MainFormOnTaskbar := True;
    Application.CreateForm(TFrm_Main, Frm_Main);
    Application.CreateForm(TFrm_Partion, Frm_Partion);
    Application.Run;
  end
  else
  begin
    ShowMessage('注意：网关已在开启中，请不要重复启动');
  end;
  ReleaseMutex(Mutex);

end.
