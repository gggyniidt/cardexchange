unit Partion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, adodb, DB, StdCtrls, Mask, DBCtrls, ExtCtrls, ComCtrls, RzEdit, uni,
  DBAccess, UniDacVcl, utils, global, Partitions, StrUtils, MemDS, Grids,
  DBGrids;

type

  // DataState = (dsAppend, dsEdit);

  TFrm_Partion = class(TForm)
    Button1: TButton;
    Button2: TButton;

    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    RadioGroup1: TRadioGroup;
    GroupBox4: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Edit4: TEdit;
    Edit5: TEdit;
    Edit6: TEdit;
    Edit7: TEdit;
    Memo1: TMemo;
    PageControl1: TPageControl;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    TabSheet6: TTabSheet;
    GroupBox2: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Edit8: TEdit;
    Edit9: TEdit;
    Edit10: TEdit;
    Edit11: TEdit;
    GroupBox3: TGroupBox;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Edit12: TEdit;
    Edit13: TEdit;
    Edit14: TEdit;
    Edit15: TEdit;
    GroupBox5: TGroupBox;
    Label16: TLabel;
    Edit16: TEdit;
    RzNumericEdit1: TRzNumericEdit;
    Label17: TLabel;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Edit3: TEdit;
    Button7: TButton;
    Label18: TLabel;
    Panel1: TPanel;
    GroupBox6: TGroupBox;
    Label19: TLabel;
    Label20: TLabel;
    CheckBox1: TCheckBox;
    UT: TUniTable;
    DBGrid1: TDBGrid;
    UT2: TUniTable;
    DataSource1: TDataSource;
    Label21: TLabel;
    Button9: TButton;
    Shape1: TShape;
    Button10: TButton;
    RzNumericEdit2: TRzNumericEdit;
    CheckBox2: TCheckBox;
    procedure RadioGroup1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button10Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);

  private
    function GenSQL1(var sql1, sql2, sql3: string): string;
    function GenSQL2(var sql1, sql2, sql3: string): string;
    procedure SavePartation(UT: TUniTable);
    procedure SaveTemplate();
    function ExistPartation(PartitionID: String): Boolean;
  public
    procedure LoadData(ID: Integer);
    procedure LoadPartition(UT: TUniTable);
    procedure LoadTemplete();
    procedure DataClear;
    procedure SetEnable(Enable: Boolean);
    function GetPartitionInfo(): TPartitionInfo;

  end;

const
  SQL1_1 =
    'select * from &AccountTableName& where &AccountField&=:AccountValue';
  SQL1_2 =
    'Update &AccountTableName& Set &MoneyField& = &MoneyField& + :MoneyValue Where &AccountField&=:AccountValue';
  SQL1_3 =
    'Insert Into &AccountTableName& (&AccountField&, &MoneyField&) Values (:AccountValue, :MoneyValue)';

  SQL2_1 =
    'Select &IDField1& From &AccountTableName& Where &AccountField&= :AccountValue';
  SQL2_2 =
    'Update &MoneyTableName& Set &MoneyField&= &MoneyField& + :MoneyValue Where &IDField2&= :IDField2Value';
  SQL2_3 =
    'Insert Into &MoneyTableName& (&IDField2&, &MoneyField&) Values (:IDField2Value, :MoneyValue)';

var
  Frm_Partion: TFrm_Partion;

implementation

{$R *.dfm}

uses main, Loger;

{ TForm1 }

procedure TFrm_Partion.Button10Click(Sender: TObject);
begin
  if UT2.RecordCount = 0 then
    Exit;
  case MessageDlg('确实要删除此模版吗?', mtConfirmation, mbYesNo, 0) of
    mrYes:
      UT2.Delete;
    mrNo:
      ;
  end;
end;

procedure TFrm_Partion.Button1Click(Sender: TObject);
begin
  if UT.State in [dsInsert, dsEdit] then
    UT.Cancel;
  Close;
end;

function TFrm_Partion.GenSQL1(var sql1, sql2, sql3: string): string;
begin
  sql1 := SQL1_1;
  sql1 := StringReplace(sql1, '&AccountTableName&', Edit5.Text, [rfReplaceAll]);
  sql1 := StringReplace(sql1, '&AccountField&', Edit6.Text, [rfReplaceAll]);

  sql2 := SQL1_2;
  sql2 := StringReplace(sql2, '&AccountTableName&', Edit5.Text, [rfReplaceAll]);
  sql2 := StringReplace(sql2, '&AccountField&', Edit6.Text, [rfReplaceAll]);
  sql2 := StringReplace(sql2, '&MoneyField&', Edit7.Text, [rfReplaceAll]);

  sql3 := SQL1_3;
  sql3 := StringReplace(sql3, '&AccountTableName&', Edit5.Text, [rfReplaceAll]);
  sql3 := StringReplace(sql3, '&AccountField&', Edit6.Text, [rfReplaceAll]);
  sql3 := StringReplace(sql3, '&MoneyField&', Edit7.Text, [rfReplaceAll]);
end;

function TFrm_Partion.GenSQL2(var sql1, sql2, sql3: string): string;
begin
  sql1 := SQL2_1;
  sql1 := StringReplace(sql1, '&AccountTableName&', Edit9.Text, [rfReplaceAll]);
  sql1 := StringReplace(sql1, '&AccountField&', Edit10.Text, [rfReplaceAll]);
  sql1 := StringReplace(sql1, '&IDField1&', Edit11.Text, [rfReplaceAll]);
  sql2 := SQL2_2;
  sql2 := StringReplace(sql2, '&MoneyTableName&', Edit13.Text, [rfReplaceAll]);
  sql2 := StringReplace(sql2, '&MoneyField&', Edit14.Text, [rfReplaceAll]);
  sql2 := StringReplace(sql2, '&IDField2&', Edit15.Text, [rfReplaceAll]);
  sql3 := SQL2_3;
  sql3 := StringReplace(sql3, '&MoneyTableName&', Edit13.Text, [rfReplaceAll]);
  sql3 := StringReplace(sql3, '&MoneyField&', Edit14.Text, [rfReplaceAll]);
  sql3 := StringReplace(sql3, '&IDField2&', Edit15.Text, [rfReplaceAll]);
end;

function TFrm_Partion.GetPartitionInfo: TPartitionInfo;
begin
  with Result do
  begin
    PartitionID := Edit1.Text;
    ServerName := Edit2.Text;
    Proportion := StrToInt(RzNumericEdit2.Text);
    DBIndex := RadioGroup1.ItemIndex;
    UseUnicode := CheckBox1.Checked;

    case DBIndex of
      0:
        begin
          DBConn1 := Edit4.Text;
          TableName1 := Edit5.Text;
          AccountField := Edit6.Text;
          MoneyField := Edit7.Text;
          GenSQL1(SQLStr1, SQLStr2, SQLStr3);
        end;
      1:
        begin
          DBConn1 := Edit8.Text;
          TableName1 := Edit9.Text;
          AccountField := Edit10.Text;
          IDField1 := Edit11.Text;

          DBConn2 := Edit12.Text;
          TableName2 := Edit13.Text;
          MoneyField := Edit14.Text;
          IDField2 := Edit15.Text;
          GenSQL2(SQLStr1, SQLStr2, SQLStr3);
        end;
      2:
        begin
          DBConn1 := Edit3.Text;
          SQLStr1 := Memo1.Text;
          SQLStr1 := StringReplace(SQLStr1, ':username', ':AccountValue',
            [rfReplaceAll, rfIgnoreCase]);
          SQLStr1 := StringReplace(SQLStr1, ':money', ':MoneyValue',
            [rfReplaceAll, rfIgnoreCase]);
          SQLStr1 := StringReplace(SQLStr1, ':orderno', ':OrderNoValue',
            [rfReplaceAll, rfIgnoreCase]);
        end;
    end;
  end;
end;

function TFrm_Partion.ExistPartation(PartitionID: String): Boolean;
var
  AQ: TUniQuery;
begin
  AQ := TUniQuery.Create(nil);
  AQ.Connection := Frm_Main.UC_Config;
  AQ.SQL.Text := 'select 1 from partition where PartitionID=' + QuotedStr
    (PartitionID);
  AQ.Open;
  if AQ.RecordCount > 0 then
    Result := True
  else
    Result := False;
  AQ.Close;
  AQ.Free;
end;

procedure TFrm_Partion.SavePartation(UT: TUniTable);
var
  PartitionInfo: TPartitionInfo;
begin

  if UT.State in [dsInsert, dsEdit] then
  begin
    PartitionInfo := GetPartitionInfo;
    if UT.State in [dsInsert] then
    begin
      if ExistPartation(PartitionInfo.PartitionID) then
      begin
        ShowMessage('此分区ID已经存在,请重新指定');
        Exit;
      end;
    end;
    with UT, PartitionInfo do
    begin
      FieldByName('PartitionID').AsString := PartitionID;
      FieldByName('ServerName').AsString := ServerName;
      FieldByName('Proportion').AsInteger := Proportion;
      FieldByName('DBIndex').AsInteger := DBIndex;
      FieldByName('UseUnicode').AsBoolean := CheckBox1.Checked;
      case DBIndex of
        0:
          begin
            FieldByName('DBConn1').AsString := DBConn1;
            FieldByName('TableName1').AsString := TableName1;
            FieldByName('AccountField').AsString := AccountField;
            FieldByName('MoneyField').AsString := MoneyField;
            FieldByName('SQLStr1').AsString := SQLStr1;
            FieldByName('SQLStr2').AsString := SQLStr2;
            FieldByName('SQLStr3').AsString := SQLStr3;
            FieldByName('AutoIns1').AsBoolean := CheckBox2.Checked;
          end;
        1:
          begin
            FieldByName('DBConn1').AsString := DBConn1;
            FieldByName('TableName1').AsString := TableName1;
            FieldByName('AccountField').AsString := AccountField;
            FieldByName('IDField1').AsString := IDField1;

            FieldByName('DBConn2').AsString := DBConn2;
            FieldByName('TableName2').AsString := TableName2;
            FieldByName('MoneyField').AsString := MoneyField;
            FieldByName('IDField2').AsString := IDField2;
            FieldByName('SQLStr1').AsString := SQLStr1;
            FieldByName('SQLStr2').AsString := SQLStr2;
            FieldByName('SQLStr3').AsString := SQLStr3;
            FieldByName('AutoIns1').AsBoolean := False;
          end;
        2:
          begin
            FieldByName('DBConn1').AsString := DBConn1;
            FieldByName('SQLStr1').AsString := SQLStr1;
            FieldByName('AutoIns1').AsBoolean := False;
          end;
      end;
    end;
    UT.Post;
    ShowMessage('请注意，本次修改需在重启服务后生效，只需点击网关上的“停止/启动”按钮即可');
    Close;
  end;
end;

procedure TFrm_Partion.SaveTemplate();
begin
  UT2.Append;
  SavePartation(UT2);
  // UT2.Post;
end;

procedure TFrm_Partion.Button2Click(Sender: TObject);
begin
  SavePartation(UT);

end;

procedure TFrm_Partion.Button3Click(Sender: TObject);
var
  p: TPartition;
  emsg: string;
  rst: Boolean;
  FLoger: TLoger;
  logrec: TLogRecord;
begin
  FLoger := TLoger.Create(config.ConfigConnStr, config.LogPort);
  p := TPartition.Create;
  p.PartitionInfo := GetPartitionInfo;
  logrec.UserName := Edit16.Text;
  logrec.PayMoney := StrToCurr(RzNumericEdit1.Text);
  logrec.YuanBao := logrec.PayMoney * p.PartitionInfo.Proportion;
  logrec.LogTime := Now;
  logrec.SendDate := Now;
  logrec.EventName := '测试';
  rst := p.ExecSQL(Edit16.Text, '测试订单号', logrec.YuanBao, emsg);
  if rst then
  begin
    logrec.Flag := 1;
    logrec.MSG := '不计入结算';
    ShowMessage('充值成功');
  end
  else
  begin
    logrec.Flag := 0;
    if emsg = '' then
      emsg := '请检查玩家帐号是否正确';
    logrec.MSG := emsg;
    ShowMessage('充值失败:' + emsg);
  end;
  FLoger.AddLog(logrec);
  FLoger.Free;
end;

procedure TFrm_Partion.Button4Click(Sender: TObject);
begin
  Edit4.Text := DBConnDlg();
end;

procedure TFrm_Partion.Button5Click(Sender: TObject);
begin
  Edit8.Text := DBConnDlg();
end;

procedure TFrm_Partion.Button6Click(Sender: TObject);
begin
  Edit12.Text := DBConnDlg();
end;

procedure TFrm_Partion.Button7Click(Sender: TObject);
begin
  Edit3.Text := DBConnDlg();
end;

procedure TFrm_Partion.Button9Click(Sender: TObject);
begin
  SaveTemplate();
end;

procedure TFrm_Partion.DataClear;
begin
  Edit1.Clear;
  Edit2.Clear;
  RzNumericEdit2.Clear;
  Edit4.Clear;
  Edit5.Clear;
  Edit6.Clear;
  Edit7.Clear;
  Edit8.Clear;
  Edit9.Clear;
  Edit10.Clear;
  Edit11.Clear;
  Edit12.Clear;
  Edit13.Clear;
  Edit14.Clear;
  Edit15.Clear;
  Memo1.Clear;
  RadioGroup1.ItemIndex := 0;
  PageControl1.ActivePageIndex := 0;
  CheckBox1.Checked := False;
end;

procedure TFrm_Partion.DBGrid1DblClick(Sender: TObject);
begin
  // LoadData();
  if UT.State in [dsEdit, dsInsert] then
    LoadTemplete;
end;

procedure TFrm_Partion.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Frm_Main.rzToolBar1.Visible := True;
  Frm_Main.UT_Partition.Close;
  Frm_Main.UT_Partition.Open;
  Frm_Main.UT_Partition.Locate('ID', UT.FieldByName('ID').AsInteger,
    [loCaseInsensitive]);
  Frm_Main.Width := 500;
  Width := 500;
end;

procedure TFrm_Partion.FormCreate(Sender: TObject);
begin
  UT2.Open;
end;

procedure TFrm_Partion.FormShow(Sender: TObject);
begin
  Frm_Main.Width := 700;
  Width := 700;
end;

procedure TFrm_Partion.LoadData(ID: Integer);
begin
  UT.Filter := 'ID=' + IntToStr(ID);
  UT.Filtered := True;
  LoadPartition(UT);
end;

procedure TFrm_Partion.LoadPartition(UT: TUniTable);
var
  idx: Integer;
begin
  DataClear;
  Edit1.Text := UT.FieldByName('PartitionID').AsString;
  Edit2.Text := UT.FieldByName('ServerName').AsString;
  RzNumericEdit2.Text := UT.FieldByName('Proportion').AsString;
  idx := UT.FieldByName('DBIndex').AsInteger;
  RadioGroup1.ItemIndex := idx;
  PageControl1.ActivePageIndex := idx;
  CheckBox1.Checked := UT.FieldByName('UseUnicode').AsBoolean;
  case idx of
    0:
      begin
        Edit4.Text := UT.FieldByName('DBConn1').AsString;
        Edit5.Text := UT.FieldByName('TableName1').AsString;
        Edit6.Text := UT.FieldByName('AccountField').AsString;
        Edit7.Text := UT.FieldByName('MoneyField').AsString;
        CheckBox2.Checked := UT.FieldByName('AutoIns1').AsBoolean;
      end;
    1:
      begin
        Edit8.Text := UT.FieldByName('DBConn1').AsString;
        Edit9.Text := UT.FieldByName('TableName1').AsString;
        Edit10.Text := UT.FieldByName('AccountField').AsString;
        Edit11.Text := UT.FieldByName('IDField1').AsString;

        Edit12.Text := UT.FieldByName('DBConn2').AsString;
        Edit13.Text := UT.FieldByName('TableName2').AsString;
        Edit14.Text := UT.FieldByName('MoneyField').AsString;
        Edit15.Text := UT.FieldByName('IDField2').AsString;
      end;
    2:
      begin
        Edit3.Text := UT.FieldByName('DBConn1').AsString;
        Memo1.Text := UT.FieldByName('SQLStr1').AsString;
        Memo1.Text := StringReplace(Memo1.Text, ':AccountValue', ':username',
          [rfReplaceAll, rfIgnoreCase]);
        Memo1.Text := StringReplace(Memo1.Text, ':MoneyValue', ':money',
          [rfReplaceAll, rfIgnoreCase]);
        Memo1.Text := StringReplace(Memo1.Text, ':OrderNoValue', ':orderno',
          [rfReplaceAll, rfIgnoreCase]);
      end;
  end;
end;

procedure TFrm_Partion.LoadTemplete;
begin
  LoadPartition(UT2);
end;

procedure TFrm_Partion.RadioGroup1Click(Sender: TObject);
begin
  PageControl1.ActivePageIndex := TRadioGroup(Sender).ItemIndex;
end;

procedure TFrm_Partion.SetEnable(Enable: Boolean);
begin
  Edit1.Enabled := Enable;
  Edit2.Enabled := Enable;
  Edit3.Enabled := Enable;
  RzNumericEdit2.Enabled := Enable;
  Edit4.Enabled := Enable;
  Edit5.Enabled := Enable;
  Edit6.Enabled := Enable;
  Edit7.Enabled := Enable;
  Edit8.Enabled := Enable;
  Edit9.Enabled := Enable;
  Edit10.Enabled := Enable;
  Edit11.Enabled := Enable;
  Edit12.Enabled := Enable;
  Edit13.Enabled := Enable;
  Edit14.Enabled := Enable;
  Edit15.Enabled := Enable;
  Memo1.Enabled := Enable;
  RadioGroup1.Enabled := Enable;
  Button4.Enabled := Enable;
  Button5.Enabled := Enable;
  Button6.Enabled := Enable;
  Button7.Enabled := Enable;
  CheckBox1.Enabled := Enable;
  CheckBox2.Enabled := Enable;
end;

end.
