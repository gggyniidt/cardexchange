unit DataDisp;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, generics.collections, StdCtrls, Sockets, utils, DB, ADODB;

type
  TTcpClientErrorEvent = procedure(Sender: TObject; SocketError: Integer)
    of object;

  TDataDisp = class(tthread)
  private
    FTcpClient: TTcpClient;
    FRan: Double;
  public
    constructor Create(Form: TForm);
    destructor Destroy;
    procedure Execute(); override;
    procedure OnTcpClientError(Sender: TObject; SocketError: Integer);
  end;

implementation

{ TDataDisp }

constructor TDataDisp.Create(Form: TForm);
begin
  inherited Create(True);
  FTcpClient := TTcpClient.Create(Form);
  FTcpClient.OnError := OnTcpClientError;
  Randomize;
  FRan := Random();
end;

destructor TDataDisp.Destroy;
begin

end;

procedure TDataDisp.Execute;
begin
  inherited;
  while not Terminated do
  begin
    sleep(10);
  end;
end;

procedure TDataDisp.OnTcpClientError(Sender: TObject; SocketError: Integer);
begin

end;

end.
