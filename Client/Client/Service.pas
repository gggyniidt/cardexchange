unit Service;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, SvcMgr, Dialogs,
  WinSock, StrUtils, Sockets, Loger, Partitions, Crypt, ExtCtrls;

type
  TSvc_CardExchangeClient = class(TService)
    TS_Work: TTcpServer;
    Timer1: TTimer;
    procedure TS_WorkAccept(Sender: TObject; ClientSocket: TCustomIpClient);
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceContinue(Sender: TService; var Continued: Boolean);
    procedure ServiceExecute(Sender: TService);
    procedure ServicePause(Sender: TService; var Paused: Boolean);
    procedure ServiceShutdown(Sender: TService);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
    procedure Timer1Timer(Sender: TObject);
  private
    FLoger: TLoger;
    procedure InitServer();
    procedure UninitServer();
  public
    function GetServiceController: TServiceController; override;
    function restartSvc: Boolean;
  end;

var
  Svc_CardExchangeClient: TSvc_CardExchangeClient;
  Partitions: TPartitions;

implementation

{$R *.DFM}

uses global, utils, DataDef;

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  Svc_CardExchangeClient.Controller(CtrlCode);
end;

function TSvc_CardExchangeClient.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TSvc_CardExchangeClient.InitServer;
var
  s: Boolean;
begin
  if LoadConfig then
    s := true
  else
    s := False;
  FLoger := TLoger.Create(config.ConfigConnStr, config.LogPort);
  // FLoger.AddLog('CEInit', '服务初始化', s, now);

  try
    Partitions := TPartitions.Create;
    // FLoger.AddLog('CEInit', '加载分区', true, now);
  except
    on E: Exception do
    begin
      // FLoger.AddLog('CEInit', '加载分区', False, now);
    end;
  end;
  try
    // TS_Work.LocalHost := '127.0.0.1';
    TS_Work.LocalPort := config.ServicePort;
    TS_Work.Open;
    // FLoger.AddLog('CEInit', '启动监听', true, now);
  except
    on E: Exception do
    begin
      // FLoger.AddLog('CEInit', '启动监听', False, now);
    end;
  end;
end;

function TSvc_CardExchangeClient.restartSvc: Boolean;
var
  sl: TStringList;
begin
  sl := TStringList.Create;
  sl.Add('net stop svc_cardexchangeclient');
  sl.Add('taskkill /f /im cardexchageclient.exe');
  sl.Add('net start svc_cardexchangeclient');
  sl.SaveToFile(AppPath + 'restart.bat');
  sl.Free;
  WinExec(pAnsichar(AnsiString(AppPath + 'restart.bat')), SW_HIDE);
end;

procedure TSvc_CardExchangeClient.ServiceContinue
  (Sender: TService; var Continued: Boolean);
begin
  // FLoger.AddLog('CEInit', '启动服务', true, now);
  while not Terminated do
  begin
    Sleep(10);
    ServiceThread.ProcessRequests(False);
  end;
end;

procedure TSvc_CardExchangeClient.ServiceExecute(Sender: TService);
begin
  while not Terminated do
  begin
    Sleep(10);
    ServiceThread.ProcessRequests(False);
  end;
end;

procedure TSvc_CardExchangeClient.ServicePause
  (Sender: TService; var Paused: Boolean);
begin
  Paused := true;
end;

procedure TSvc_CardExchangeClient.ServiceShutdown(Sender: TService);
begin
  Status := csStopped;
  ReportStatus();
end;

procedure TSvc_CardExchangeClient.ServiceStart
  (Sender: TService; var Started: Boolean);
begin
  InitServer;
end;

procedure TSvc_CardExchangeClient.ServiceStop
  (Sender: TService; var Stopped: Boolean);
begin
  // TS_Work.Close;
  Stopped := true;
  UninitServer;
end;

procedure TSvc_CardExchangeClient.Timer1Timer(Sender: TObject);
begin
  if TS_Work.Active then
  begin
    TS_Work.Close;
    TS_Work.Open;
    restartSvc;
  end;
end;

procedure TSvc_CardExchangeClient.TS_WorkAccept(Sender: TObject;
  ClientSocket: TCustomIpClient);
var
  rcep, scep: TCEPtl;
  RNetPck, SNetPck: TNetPck;
  ret: Integer;
  Res: Boolean;
  pid: Integer;
  p: TPartition;
  Money: Currency;
  account: string;
  orderno: string;
  len: Integer;
  LogRec: TLogRecord;
  emsg: string;
begin
  if config.FixedServer then
  begin
    if not ContainsText(config.ServerIP, ClientSocket.RemoteHost) then
    // if ClientSocket.RemoteHost <> config.ServerIP then
    begin
      FLoger.AddLog('拦截', '非法服务器IP:' + ClientSocket.RemoteHost, true, now);
      ClientSocket.Disconnect;
      exit;
    end;
  end;

  while (true) do
  begin
    if not ClientSocket.WaitForData(1000) then // 等待数据
      Continue;

    ret := ClientSocket.ReceiveBuf(RNetPck, sizeof(TNetPck), 0);
    if ret <= 0 then // 服务端已断开
    begin
      ClientSocket.Close;
      Break;
    end;
    // 解密                                           2
    if not DecyptCEPT(RNetPck, rcep, CryptKey) then
    begin
      scep.which := cewSucc;
      scep.u.Flag := 2;
      scep.u.Succmsg := '数据验证错误';
      LogRec.Flag := scep.u.Flag;
      LogRec.MSG := scep.u.Succmsg;
      FLoger.AddLog(LogRec);
      EncyptCEPT(scep, SNetPck, CryptKey);
      len := ClientSocket.SendBuf(scep, sizeof(TNetPck), 0);
      ClientSocket.Close;
      Break;
    end;

    /// ///////////////
    ///
    case rcep.which of
      cewData: // 正常数据
        begin
          try
            LogRec.CodeNum := rcep.u.Rec.CodeNum;
            LogRec.UserName := rcep.u.Rec.UserName;
            LogRec.PayMoney := rcep.u.Rec.PayMoney;
            LogRec.SendDate := rcep.u.Rec.SendDate;
            LogRec.orderId := rcep.u.Rec.orderId;
            if rcep.u.Rec.classid = 0 then
              LogRec.EventName := '充值'
            else if rcep.u.Rec.classid = 1 then
              LogRec.EventName := '补发';
            LogRec.LogTime := now;
            pid := rcep.u.Rec.CodeNum;
            account := rcep.u.Rec.UserName;
            orderno:=rcep.u.Rec.orderId;
            p := Partitions.Partition[pid];
            if p = nil then
            begin
              scep.which := cewSucc;
              scep.u.Flag := 2;
              scep.u.Succmsg := '找不到分区信息';
              LogRec.Flag := scep.u.Flag;
              LogRec.MSG := scep.u.Succmsg;
              FLoger.AddLog(LogRec);
              EncyptCEPT(scep, SNetPck, CryptKey);
              len := ClientSocket.SendBuf(SNetPck, sizeof(TNetPck), 0);
              ClientSocket.Close;
              Break;
            end;
            if FLoger.ISSended(rcep.u.Rec.orderId) then
            begin
              scep.which := cewSucc;
              scep.u.Flag := 2;
              scep.u.Succmsg := '重复订单';
              LogRec.Flag := scep.u.Flag;
              LogRec.MSG := scep.u.Succmsg;
              // FLoger.AddLog(LogRec);
              EncyptCEPT(scep, SNetPck, CryptKey);
              len := ClientSocket.SendBuf(SNetPck, sizeof(TNetPck), 0);
              ClientSocket.Close;
              Break;
            end;
            if config.ServicePassword = rcep.u.Rec.postKey then
            begin
              Money := p.PartitionInfo.Proportion * rcep.u.Rec.PayMoney;
              LogRec.YuanBao := Money;
              Res := p.ExecSQL(account, orderno, Money, emsg);
              // Res := true;
              if Res then
              begin
                scep.which := cewSucc;
                scep.u.Flag := 1;
                scep.u.Succmsg := '成功';
                LogRec.Flag := scep.u.Flag;
                LogRec.MSG := scep.u.Succmsg;
                if rcep.u.Rec.classid = 0 then
                  LogRec.MSG := '请在后台复核'
                else if rcep.u.Rec.classid = 1 then
                  LogRec.MSG := '不计入结算';
                FLoger.AddLog(LogRec);
                EncyptCEPT(scep, SNetPck, CryptKey);
                len := ClientSocket.SendBuf(SNetPck, sizeof(TNetPck), 0);
                ClientSocket.Close;
                LogRec.Flag := len;
                Break;
              end
              else
              begin
                scep.which := cewSucc;
                scep.u.Flag := 2;
                if emsg <> '' then
                  scep.u.ErrMsg := emsg
                else
                  scep.u.ErrMsg := '请检查玩家帐号是否正确';
                LogRec.Flag := scep.u.Flag;
                LogRec.MSG := scep.u.Succmsg;
                FLoger.AddLog(LogRec);
                EncyptCEPT(scep, SNetPck, CryptKey);
                len := ClientSocket.SendBuf(SNetPck, sizeof(SNetPck), 0);
                ClientSocket.Close;
                Break;
              end;
            end
            else
            begin
              scep.which := cewSucc;
              scep.u.Flag := 2;
              scep.u.ErrMsg := '服务密码错误';
              LogRec.Flag := scep.u.Flag;
              LogRec.MSG := scep.u.Succmsg;
              FLoger.AddLog(LogRec);
              EncyptCEPT(scep, SNetPck, CryptKey);
              len := ClientSocket.SendBuf(SNetPck, sizeof(SNetPck), 0);
              ClientSocket.Close;
              Break;
            end;
          except
            on E: Exception do
            begin

              scep.which := cewSucc;
              scep.u.Flag := 2;
              scep.u.ErrMsg := '客户端未知错误';
              LogRec.Flag := scep.u.Flag;
              LogRec.MSG := scep.u.Succmsg;
              FLoger.AddLog(LogRec);
              EncyptCEPT(scep, SNetPck, CryptKey);
              len := ClientSocket.SendBuf(SNetPck, sizeof(SNetPck), 0);
              ClientSocket.Close;
              Break;
            end;
          end;
        end;
    else // 错误数据
      begin
        scep.which := cewError;
        scep.u.Flag := 2;
        scep.u.ErrMsg := '错误的数据封包';
        LogRec.Flag := scep.u.Flag;
        LogRec.MSG := scep.u.Succmsg;
        FLoger.AddLog(LogRec);
        EncyptCEPT(scep, SNetPck, CryptKey);
        len := ClientSocket.SendBuf(SNetPck, sizeof(SNetPck), 0);
        ClientSocket.Close;
        Break;
      end;
    end;
  end;
end;

procedure TSvc_CardExchangeClient.UninitServer;
begin
  // FLoger.AddLog('CEFinal', '停止服务', true, now);
  TS_Work.Close;
  Partitions.Free;
  Sleep(10);
  FLoger.Free;
end;

end.
