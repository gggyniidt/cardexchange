unit Partitions;

interface

uses Windows, SysUtils, ShellAPI, Forms, Dialogs, classes, sockets,
  generics.collections, SyncObjs, adodb, UniDacVcl,
  Uni, Winsock, Utils, SQLServerUniProvider, MySQLUniProvider, UniProvider,
  ODBCUniProvider, StrUtils, AccessUniProvider;

type
  TPartitionInfo = record
    ID: Integer;
    PartitionID: string;
    Proportion: Integer;
    DBIndex: Integer;
    DBConn1: String;
    DBConn2: String;
    TableName1: String;
    TableName2: String;
    AccountField: String;
    MoneyField: String;
    IDField1: String;
    IDField2: String;
    SQLStr1: string;
    SQLStr2: string;
    SQLStr3: string;
    ServerName: string;
    UseUnicode: Boolean;
    AutoIns1: Boolean;
  end;

  TPartition = class(TObject)
  private
    UC1: TUniConnection;
    UC2: TUniConnection;
    UQ1: TUniQuery;
    UQ2: TUniQuery;
  public
    PartitionInfo: TPartitionInfo;
    constructor Create();
    destructor Destroy; override;
    function ExecSQL(AccountValue, OrderNoValue: string; MoneyValue: Currency;
      var Msg: string): Boolean;
  end;

  TPartitionDic = TDictionary<Integer, TPartition>;

  TPartitions = class(TObject)
  private
    FPartionDic: TPartitionDic;
    function GetPartition(PartitionID: Integer): TPartition;
    procedure GetPartitions;
  public
    constructor Create();
    destructor Destroy; override;
    property Partition[PartitionID: Integer]: TPartition read GetPartition;
  end;

implementation

uses global;
{ TPartition }

constructor TPartition.Create();
begin
  UC1 := TUniConnection.Create(nil);
  UQ1 := TUniQuery.Create(nil);
  UQ1.Connection := UC1;

  UC2 := TUniConnection.Create(nil);
  UQ2 := TUniQuery.Create(nil);
  UQ2.Connection := UC2;
end;

destructor TPartition.Destroy;
begin
  FreeAndNil(UQ1);
  FreeAndNil(UC1);

  FreeAndNil(UQ2);
  FreeAndNil(UC2);

  inherited;
end;

function TPartition.ExecSQL(AccountValue, OrderNoValue: string;
  MoneyValue: Currency; var Msg: string): Boolean;
var
  IDField1Value: string;
  cnt: Integer;
begin
  Msg := '';
  result := False;
  case PartitionInfo.DBIndex of
    0:
      begin
        // 原始代码别改
        // ConnectDB(UC1, PartitionInfo.DBConn1, PartitionInfo.UseUnicode);
        // UQ1.Close;
        // UQ1.SQL.Text := PartitionInfo.SQLStr1;
        // UQ1.Params.ParamByName('AccountValue').AsString := AccountValue;
        // UQ1.Execute;
        // cnt := UQ1.RecordCount;
        // UQ1.Close;
        // if cnt > 0 then
        // UQ1.SQL.Text := PartitionInfo.SQLStr2 // 更新
        // else
        // UQ1.SQL.Text := PartitionInfo.SQLStr3; // 插入
        // UQ1.Params.ParamByName('AccountValue').AsString := AccountValue;
        // UQ1.Params.ParamByName('MoneyValue').AsCurrency := MoneyValue;
        // UQ1.Execute;
        // if UQ1.RowsAffected > 0 then
        // result := True
        // else
        // result := False;
        // UQ1.Close;
        // UC1.Close;
        // 原始代码别改
        try
          try
            ConnectDB(UC1, PartitionInfo.DBConn1, PartitionInfo.UseUnicode);
            UQ1.Close;
            UQ1.SQL.Text := PartitionInfo.SQLStr1;
            UQ1.Params.ParamByName('AccountValue').AsString := AccountValue;
            UQ1.Execute;
            cnt := UQ1.RecordCount;
            UQ1.Close;
            if cnt > 0 then
            begin
              UQ1.SQL.Text := PartitionInfo.SQLStr2; // 更新
              UQ1.Params.ParamByName('AccountValue').AsString := AccountValue;
              UQ1.Params.ParamByName('MoneyValue').AsCurrency := MoneyValue;
              UQ1.Execute;
            end
            else
            begin
              if PartitionInfo.AutoIns1 then // 如果有自动插入选项
              begin
                UQ1.SQL.Text := PartitionInfo.SQLStr3; // 插入
                UQ1.Params.ParamByName('AccountValue').AsString := AccountValue;
                UQ1.Params.ParamByName('MoneyValue').AsCurrency := MoneyValue;
                UQ1.Execute;
              end;
            end;
            if UQ1.RowsAffected > 0 then
              result := True
            else
              result := False;
            UQ1.Close;
          except
            on E: Exception do
            begin
              UQ1.Close;
              UC1.Close;
              result := False;
              Msg := E.Message;
            end;
          end;
        finally
          UQ1.Close;
          UC1.Close;
        end;
      end;
    1:
      begin
        try
          try
            // SQL2_2 = 'Update &MoneyTableName& Set &MoneyField&= &MoneyField& + :MoneyValue Where &IDField2&= :IDField2Value';
            // SQL2_3 = 'Insert Into &MoneyTableName& (&IDField2&, &MoneyField&) Values (:IDField2Value, :MoneyValue)';
            ConnectDB(UC1, PartitionInfo.DBConn1, PartitionInfo.UseUnicode);
            UQ1.Close;
            UQ1.SQL.Text := PartitionInfo.SQLStr1; // select 表一
            UQ1.Params.ParamByName('AccountValue').AsString := AccountValue;
            UQ1.ExecSQL;

            if UQ1.RecordCount > 0 then // 若存在该用户
            begin
              IDField1Value := UQ1.FieldByName(PartitionInfo.IDField1).AsString;
              // 取ID
              if IDField1Value <> '' then // 取到了ID
              begin
                UQ1.Close;
                UC1.Close;
                ConnectDB(UC2, PartitionInfo.DBConn2, PartitionInfo.UseUnicode);
                UQ2.Close;
                UQ2.SQL.Text := PartitionInfo.SQLStr2;
                UQ2.Params.ParamByName('IDField2Value').AsString :=
                  IDField1Value;
                UQ2.Params.ParamByName('MoneyValue').AsCurrency := MoneyValue;
                UQ2.Execute;

                if UQ2.RowsAffected <= 0 then // 没有更新成功即不存在记录
                begin
                  UQ2.Close;
                  UQ2.SQL.Text := PartitionInfo.SQLStr3; // Insert
                  UQ2.Params.ParamByName('IDField2Value').AsString :=
                    IDField1Value;
                  UQ2.Params.ParamByName('MoneyValue').AsCurrency := MoneyValue;
                  UQ2.Execute;

                  if UQ2.RowsAffected > 0 then // 插入成功
                    result := True
                  else
                    result := False; // 插入失败
                  UQ2.Close;
                  UC2.Close;
                end
                else
                begin
                  UQ2.Close;
                  UC2.Close;
                  result := True; // 更新成功
                end;
              end
              else
              begin // 取不到ID
                UQ1.Close;
                UC1.Close;
                result := False;
              end;
            end
            else
            begin // 不存在用户
              UQ1.Close;
              UC1.Close;
              result := False;
            end;
          except
            on E: Exception do
            begin
              UQ1.Close;
              UQ2.Close;
              UC1.Close;
              UC2.Close;
              result := False;
              Msg := E.Message;
            end;
          end;
        finally
          UQ1.Close;
          UQ2.Close;
          UC1.Close;
          UC2.Close;
        end;
      end;
    2:
      begin
        try
          try
            ConnectDB(UC1, PartitionInfo.DBConn1, PartitionInfo.UseUnicode);
            UQ1.Close;
            UQ1.SQL.Text := PartitionInfo.SQLStr1;
            UQ1.Params.ParamByName('AccountValue').AsString := AccountValue;
            UQ1.Params.ParamByName('MoneyValue').AsCurrency := MoneyValue;
            if Pos('', UQ1.SQL.Text) > 0 then
              UQ1.Params.ParamByName('OrderNoValue').AsString := OrderNoValue;
            UQ1.ExecSQL;
            if UQ1.RowsAffected > 0 then
              result := True
            else
              result := False;
            UC1.Close;
            UQ1.Close;
          except
            on E: Exception do
            begin
              UQ1.Close;
              UC1.Close;
              result := False;
              Msg := E.Message;
            end;
          end;
        finally
          UQ1.Close;
          UC1.Close;
        end;
      end;
  end;
end;

{ TPartitions }

constructor TPartitions.Create;
begin
  FPartionDic := TPartitionDic.Create;
  GetPartitions;
end;

destructor TPartitions.Destroy;
begin
  FPartionDic.Free;
  inherited;
end;

function TPartitions.GetPartition(PartitionID: Integer): TPartition;
begin
  if FPartionDic.ContainsKey(PartitionID) then
    result := FPartionDic.Items[PartitionID]
  else
    result := nil;
end;

procedure TPartitions.GetPartitions();

var
  UC: TUniConnection;
  UT: TUniTable;
  Partition: TPartition;
begin
  UC := TUniConnection.Create(nil);
  UT := TUniTable.Create(nil);
  try
    try
      ConnectDB(UC, config.ConfigConnStr);
      UT.Connection := UC;
      UT.TableName := 'Partition';
      UT.Open;

      while not UT.Eof do
      begin
        Partition := TPartition.Create();
        with Partition.PartitionInfo, UT do
        begin
          ID := FieldByName('PartitionID').AsInteger;
          ServerName := FieldByName('ServerName').AsString;
          Proportion := FieldByName('Proportion').AsInteger;
          DBIndex := FieldByName('DBIndex').AsInteger;
          UseUnicode := FieldByName('UseUnicode').AsBoolean;

          case DBIndex of
            0:
              begin
                DBConn1 := UT.FieldByName('DBConn1').AsString;
                TableName1 := UT.FieldByName('TableName1').AsString;
                AccountField := UT.FieldByName('AccountField').AsString;
                MoneyField := UT.FieldByName('MoneyField').AsString;
                SQLStr1 := UT.FieldByName('SQLStr1').AsString;
                SQLStr2 := UT.FieldByName('SQLStr2').AsString;
                SQLStr3 := UT.FieldByName('SQLStr3').AsString;
                AutoIns1 := UT.FieldByName('AutoIns1').AsBoolean;
              end;
            1:
              begin
                DBConn1 := UT.FieldByName('DBConn1').AsString;
                TableName1 := UT.FieldByName('TableName1').AsString;
                AccountField := UT.FieldByName('AccountField').AsString;
                IDField1 := UT.FieldByName('IDField1').AsString;

                DBConn2 := UT.FieldByName('DBConn2').AsString;
                TableName2 := UT.FieldByName('TableName2').AsString;
                MoneyField := UT.FieldByName('MoneyField').AsString;
                IDField2 := UT.FieldByName('IDField2').AsString;

                SQLStr1 := UT.FieldByName('SQLStr1').AsString;
                SQLStr2 := UT.FieldByName('SQLStr2').AsString;
                SQLStr3 := UT.FieldByName('SQLStr3').AsString;
              end;
            2:
              begin
                DBConn1 := UT.FieldByName('DBConn1').AsString;
                SQLStr1 := UT.FieldByName('SQLStr1').AsString;
              end;
          end;
        end;

        FPartionDic.Add(Partition.PartitionInfo.ID, Partition);
        UT.Next;
      end;
      UT.Close;
      UC.Close;
    except
    end;
  finally
    UT.Free;
    UC.Free
  end;
end;

end.
