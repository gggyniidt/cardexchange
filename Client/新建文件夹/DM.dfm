object Frm_DM: TFrm_DM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 423
  Width = 460
  object AccessUniProvider1: TAccessUniProvider
    Left = 152
    Top = 8
  end
  object ODBCUniProvider1: TODBCUniProvider
    Left = 224
    Top = 8
  end
  object UC_1: TUniConnection
    Left = 152
    Top = 272
  end
  object UC_2: TUniConnection
    Left = 224
    Top = 272
  end
  object UCD_1: TUniConnectDialog
    DatabaseLabel = #25968#25454#24211
    PortLabel = #31471#21475
    ProviderLabel = #25968#25454#24211#31867#22411
    Caption = #36830#25509#25968#25454#24211
    UsernameLabel = #29992#25143#21517
    PasswordLabel = #23494#30721
    ServerLabel = #26381#21153#22120'IP'
    ConnectButton = #36830#25509
    CancelButton = #21462#28040
    LabelSet = lsCustom
    Left = 152
    Top = 336
  end
  object UCD_2: TUniConnectDialog
    DatabaseLabel = 'Database'
    PortLabel = 'Port'
    ProviderLabel = 'Provider'
    Caption = 'Connect'
    UsernameLabel = 'User Name'
    PasswordLabel = 'Password'
    ServerLabel = 'Server'
    ConnectButton = 'Connect'
    CancelButton = 'Cancel'
    Left = 224
    Top = 336
  end
  object MySQLUniProvider1: TMySQLUniProvider
    Left = 304
    Top = 272
  end
  object SQLServerUniProvider1: TSQLServerUniProvider
    Left = 304
    Top = 336
  end
end
