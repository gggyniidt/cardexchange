unit DM;

interface

uses
  SysUtils, Classes, DB, ADODB, utils, UniProvider, ODBCUniProvider,
  AccessUniProvider, DBAccess, Uni, MemDS, UniDacVcl, SQLServerUniProvider,
  MySQLUniProvider;

type
  TFrm_DM = class(TDataModule)
    AccessUniProvider1: TAccessUniProvider;
    ODBCUniProvider1: TODBCUniProvider;
    UC_1: TUniConnection;
    UC_2: TUniConnection;
    UCD_1: TUniConnectDialog;
    UCD_2: TUniConnectDialog;
    MySQLUniProvider1: TMySQLUniProvider;
    SQLServerUniProvider1: TSQLServerUniProvider;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Frm_DM: TFrm_DM;

implementation

{$R *.dfm}

uses global;

procedure TFrm_DM.DataModuleCreate(Sender: TObject);
begin
  //LoadConfig();

  UC_1.ConnectDialog := UCD_1;
  UC_2.ConnectDialog := UCD_2;

end;

end.
