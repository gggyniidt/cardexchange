program Project1;

uses
  Forms,
  Unit3 in 'Unit3.pas' {Form3},
  DBScanner in '..\Server\Server\DBScanner.pas',
  Utils in '..\Common\Utils.pas',
  Unit2 in 'Unit2.pas' {Form2},
  global in '..\Server\global.pas',
  Loger in '..\Common\Loger.pas',
  DataDef in '..\Common\DataDef.pas',
  Crypt in '..\Common\Crypt.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm2, Form2);
  Application.Run;
end.
