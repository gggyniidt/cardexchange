unit Unit3;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DBScanner, Utils;

type
  TForm3 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    { Private declarations }
  public
    function OnDBChange(var Rec: TDBRecord): Boolean;
  end;

var
  Form3: TForm3;

implementation

{$R *.dfm}

var
  FDS: tdbscanner;

procedure TForm3.Button1Click(Sender: TObject);

begin
  FDS := tdbscanner.Create(
    'ProviderName=SQL Server;Username=sa;Password=gyi@134;Server=127.0.0.1;Port=0;Database=touspay_data');
  FDS.OnDBChange := OnDBChange;
  FDS.TimeSpace := 1000;

end;

procedure TForm3.Button2Click(Sender: TObject);
begin
  FDS.Resume;
end;

procedure TForm3.Button3Click(Sender: TObject);
begin
  FDS.Terminate;
end;

procedure TForm3.Button4Click(Sender: TObject);
begin
  FDS.Free;
end;

function TForm3.OnDBChange(var Rec: TDBRecord): Boolean;
begin
  Memo1.Lines.Add(IntToStr(Memo1.Lines.Count)
      + '--' + Rec.num + ':' + Rec.UserName);
  if Rec.id mod 2 = 0 then // ż��ʧ��
  begin
    Result := false;
    Rec.msg := 'ʧ��';
  end
  else
    Result := true;
end;

end.
