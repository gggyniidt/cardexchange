unit Unit2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, SvcMgr, Dialogs,
  Sockets, forms, Loger, DBScanner, Utils, WinSock, DataDef, Crypt, regware4,
  ExtCtrls, StrUtils, StdCtrls;

type
  TForm2 = class(TForm)
    Button1: TButton;
    RegIV: TRegware4;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    FLoger: TLoger;
    FDS: TDBScanner;
    s: TSocket;
    procedure InitServer();
    procedure UninitServer();

    function SendData(netpak: TNetPck): Boolean;
    function RecvData(var netpak: TNetPck): Integer;
    function InitSocket(IP: AnsiString; Port: Word): Boolean;
    function UninitSocket: Boolean;
  public
    function OnDBChange(var Rec: TDBRecord): Boolean;
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

uses global;

procedure TForm2.Button1Click(Sender: TObject);
begin
  InitServer;
end;

procedure TForm2.Button2Click(Sender: TObject);
begin
  UninitServer;
end;

procedure TForm2.InitServer;
var
  s: Boolean;
begin
  if LoadConfig then
    s := true
  else
    s := False;
  FLoger := TLoger.Create(config.ConfigConnStr, config.LogPort);
  FLoger.AddLog('CEInit', '服务初始化', s, now);

  try
    FDS := TDBScanner.Create(config.ConnStr);
    FDS.OnDBChange := OnDBChange;
    FDS.TimeSpace := config.ScanTime;

    FDS.Start;
    FLoger.AddLog('CEDBScan', '数据扫描', true, now);

  except
    FLoger.AddLog('CEDBScan', '数据扫描', False, now);
  end;
end;

function TForm2.InitSocket(IP: AnsiString; Port: Word): Boolean;
var
  sa: TWSAData;
  ad: sockaddr_in;
  timeout: Integer;
  ret, len, err: Integer;
  ul: Integer;
  tm: timeval;
  fdset: TFDSet;
begin
  ret := WSAStartup($0202, sa);
  if ret <> 0 then
  begin
    Result := False;
    Exit;
  end;

  s := socket(PF_INET, SOCK_STREAM, IPPROTO_IP);
  if s = INVALID_SOCKET then
  begin
    Result := False;
    WSACleanup;
    Exit;
  end;

  timeout := 20000; // 发送超时
  ret := setsockopt(s, SOL_SOCKET, SO_SNDTIMEO, @timeout, sizeof(timeout));
  if ret <> 0 then
  begin
    Result := False;
    WSACleanup;
    Exit;
  end;

  timeout := 20000; // 接收超时
  ret := setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, @timeout, sizeof(timeout));
  if ret <> 0 then
  begin
    Result := False;
    WSACleanup;
    Exit;
  end;

  ul := 1;
  ret := ioctlsocket(s, FIONBIO, ul); // 设置为非阻塞模式
  if ret <> 0 then
  begin
    Result := False;
    WSACleanup;
    Exit;
  end;

  ad.sin_family := PF_INET;
  ad.sin_port := htons(Port);
  ad.sin_addr.S_addr := inet_addr(pansichar(IP));

  ret := connect(s, ad, sizeof(ad));
  if (ret = -1) then
  begin
    tm.tv_sec := 20; // connect超时3秒
    tm.tv_usec := 0;
    FD_ZERO(fdset);
    FD_SET(s, fdset);
    ret := select(s + 1, nil, @fdset, nil, @tm);
    if (ret > 0) then
    begin
      len := sizeof(Integer);
      getsockopt(s, SOL_SOCKET, SO_ERROR, @err, len);

      if (err = 0) then
        Result := true
      else
        Result := False;
    end
    else
      Result := False;
  end
  else
    Result := true;

  ul := 0;
  ret := ioctlsocket(s, FIONBIO, ul); // 设置为阻塞模式
  if ret <> 0 then
  begin
    Result := False;
    WSACleanup;
    Exit;
  end;

  Result := true;
end;

function TForm2.OnDBChange(var Rec: TDBRecord): Boolean;
var
  r: TLogRecord;
  rceptl, sceptl: TCEPtl;
  i: Integer;
  errcode: Integer;
  Rnetpck, Snetpck: TNetPck;
begin
  try
    if RegIV.Expired then
    begin
      Rec.flag := 2;
      Rec.msg := '试用期已过,未发送';

      r.LogTime := now;
      r.EventName := '注册';
      r.CodeNum := Rec.CodeNum;
      r.UserName := Rec.UserName;
      r.PayMoney := Rec.PayMoney;
      r.flag := Rec.flag;
      r.SendDate := Rec.SendDate;
      r.OrderID := Rec.OrderID;
      r.msg := Rec.msg;
      r.LogTime := now;
      r.ClientIP := Rec.svrIp;
      FLoger.AddLog(r);
      Result := true;
      Exit;
    end;

    if Rec.classid = 0 then // '充值'
    begin
      if not FDS.RealBill(Rec.Num) then
      begin
        Rec.flag := 2;
        Rec.msg := '假订单';

        r.EventName := '充值';
        r.msg := '假订单';
        r.LogTime := now;
        r.CodeNum := Rec.CodeNum;
        r.UserName := Rec.UserName;
        r.PayMoney := Rec.PayMoney;
        r.flag := Rec.flag;
        r.SendDate := Rec.SendDate;
        r.OrderID := Rec.OrderID;
        r.msg := Rec.msg;
        r.LogTime := now;
        r.ClientIP := Rec.svrIp;
        FLoger.AddLog(r);
        Result := true;
        Exit;
      end;
    end;

    sceptl.which := cewData;
    sceptl.u.Rec := Rec;
    // 加密
    EncyptCEPT(sceptl, Snetpck, CryptKey);

    for i := 0 to config.ResendTimes - 1 do
    begin
      // 私有网络不发包
//      if (StartsText('192.168', Rec.svrIp) or StartsText('127.0', Rec.svrIp))
//        then
//      begin
//        Rec.flag := 2;
//        Rec.msg := '连接' + Rec.svrIp + ':' + inttostr(Rec.svrPort) + '失败:私有地址';
//        Break;
//      end;
      if not InitSocket(Rec.svrIp, Rec.svrPort) then
      begin
        Rec.flag := 2;
        Rec.msg := '连接' + Rec.svrIp + ':' + inttostr(Rec.svrPort) + '失败';
        Continue;
      end;
      if not SendData(Snetpck) then
      begin
        Rec.flag := 2;
        Rec.msg := '发送数据到' + Rec.svrIp + ':' + inttostr(Rec.svrPort) + '失败';
        Continue;
      end;
      errcode := RecvData(Rnetpck);
      if errcode <= 0 then
      begin
        Rec.flag := 2;
        Rec.msg := '到' + Rec.svrIp + ':' + inttostr(Rec.svrPort)
          + '的连接意外关闭' + inttostr(errcode);
        Continue;
      end;
      if errcode <> sizeof(TNetPck) then
      begin
        Rec.flag := 2;
        Rec.msg := '接收到' + Rec.svrIp + ':' + inttostr(Rec.svrPort) + '的错误的数据封包';
        Continue;
      end;
      /// 解密
      DecyptCEPT(Rnetpck, rceptl, CryptKey);
      case rceptl.which of
        cewError:
          begin
            Rec.flag := 2;
            Rec.msg := '客户端数据处理失败:' + rceptl.u.ErrMsg + inttostr(errcode);
            Continue;
          end;
        cewSucc:
          begin
            Rec.flag := rceptl.u.flag;
            Rec.msg := rceptl.u.Succmsg;
            UninitSocket;
            Break;
          end;
      else
        begin
          Rec.flag := 2;
          Rec.msg := '未知格式的数据包:' + rceptl.u.ErrMsg + inttostr(errcode);
          Continue;
        end;
      end;
    end;
    r.CodeNum := Rec.CodeNum;
    r.UserName := Rec.UserName;
    r.PayMoney := Rec.PayMoney;
    r.ClientIP := Rec.svrIp;
    r.flag := Rec.flag;
    r.SendDate := Rec.SendDate;
    r.OrderID := Rec.OrderID;
    if Rec.classid = 0 then
    begin
      r.EventName := '充值';
      r.msg := '请在后台复核';
    end
    else if Rec.classid = 1 then
    begin
      r.EventName := '补发';
      r.msg := '不计入结算';
    end;
    if Rec.flag <> 1 then
      r.msg := Rec.msg;
    r.LogTime := now;
    FLoger.AddLog(r);
    Result := true;
  except
    on E: Exception do
    begin
      r.flag := 2;
      r.msg := E.Message + '异常';
      r.LogTime := now;
      r.ClientIP := Rec.svrIp;
      if Rec.classid = 0 then
        r.EventName := '充值'
      else if Rec.classid = 1 then
        r.EventName := '补发';
      FLoger.AddLog(r);
      Result := true;
    end;
  end;
end;

function TForm2.RecvData(var netpak: TNetPck): Integer;
begin
  Result := recv(s, netpak, sizeof(TNetPck), 0);
end;

function TForm2.SendData(netpak: TNetPck): Boolean;
var
  sendLen: Integer;
begin
  sendLen := send(s, netpak, sizeof(TNetPck), 0);
  if sendLen < 0 then
  begin
    Result := False;
    WSACleanup;
    Exit;
  end
  else
    Result := true;
end;

procedure TForm2.UninitServer;
begin
  FLoger.AddLog('CEFinal', '停止服务', true, now);
  FDS.Terminate;
  FDS.WaitFor;
  FLoger.Free;
end;

function TForm2.UninitSocket: Boolean;
begin
  try
    closesocket(s);
    WSACleanup;
    Result := true;
  except
    Result := False;
  end;
end;

end.
